var express = require('express');
var fs = require('fs');
var http = require('http');
var sql = require('mssql');
var app = express();
var config_file = require('./config.json');
var compression = require('compression');


var config = {
    user: 'astroda01',
    password: 'AstroDa@16',
    server: '172.18.56.180',
    database: 'IMPODS01',
    port: 1433
}


app.use(express.static(__dirname + '/public'));
app.use(compression());





app.get('/:KeyString/:ValueString', function (req, res) {
	
	res.writeHead(200,{"Content-Type" : "application/json"});
	
	$strVal=null;
	if (req.params.KeyString.toUpperCase()==='SEARCHBYCONTACT' || req.params.KeyString.toUpperCase()==='SEARCHBYEMAIL') {
		$strVal="'" + req.params.ValueString + "'";
	}
	else {
			$strVal=req.params.ValueString ;
	}
	
	var str=config_file[req.params.KeyString.toUpperCase()].query 
	+ "(" + $strVal + ") " 
	+ config_file[req.params.KeyString.toUpperCase()].orderby;
	
	console.log(str);
	
	console.log('/GET ' + req.connection.remoteAddress);
	//console.log(req.connection);
	
	fetchDataDB(config_file[req.params.KeyString.toUpperCase()].key,str, function(err, result) {
		
		if(err) {
			res.end(err);
		}
		else {
			res.end(result);
		}
	});
	
    
})

app.get('/:KeyString/:ValueString1/:ValueString2', function (req, res) {
	
	res.writeHead(200,{"Content-Type" : "application/json"});
	
	var str=config_file[req.params.KeyString.toUpperCase()].query 
	+ "(" + req.params.ValueString1 + "," + req.params.ValueString2 + ") " 
	+ config_file[req.params.KeyString.toUpperCase()].orderby;
	
	//console.log(str);
	
	fetchDataDB(config_file[req.params.KeyString.toUpperCase()].key,str, function(err, result) {
		
		if(err) {
			res.end(err);
		}
		else {
			res.end(result);
		}
	});
	
    
})

app.get('/:KeyString/:ValueString1/:ValueString2/:ValueString3', function (req, res) {
	
	res.writeHead(200,{"Content-Type" : "application/json"});
	
	var str=config_file[req.params.KeyString.toUpperCase()].query 
	+ "(" + req.params.ValueString1 + "," + req.params.ValueString2 + "," + req.params.ValueString3 + ") " 
	+ config_file[req.params.KeyString.toUpperCase()].orderby;
	
	//console.log(str);
	
	fetchDataDB(config_file[req.params.KeyString.toUpperCase()].key,str, function(err, result) {
		
		if(err) {
			res.end(err);
		}
		else {
			res.end(result);
		}
	});
	
    
})


app.get('/:SLA/:ValueString', function (req, res) {
	
	res.writeHead(200,{"Content-Type" : "application/json"});
	
	$strVal=null;
	
	if (req.params.KeyString.toUpperCase()==='EMAIL') {
		try {  
			var str = fs.readFileSync('./email.sql', 'utf8');
			console.log(str);    
		} catch(e) {
			console.log('Error:', e.stack);
		}
	}
	
	console.log(str);
	
	console.log('/GET ' + req.connection.remoteAddress);
	//console.log(req.connection);
	
	fetchDataDBgenesys("email_stats",str, function(err, result) {
		
		if(err) {
			res.end(err);
		}
		else {
			res.end(result);
		}
	});
	
    
})



app.listen(8082, function () {

    console.log("server running at http://IP_ADDRESS:8082/")
});

function fetchDataDB(keyStr,sqlstring,cb) {
	var conn = new sql.Connection(config);
    conn.connect().then(function () {
	    var req = new sql.Request(conn);
	    req.query(sqlstring).then(function (recordset) {
			//console.log(keyStr);
			var obj={};
			obj[keyStr]=recordset;
			var json=JSON.stringify(obj);
			
			//console.log(json);
			cb(null,json);
            conn.close();
        })
        .catch(function (err) {
			//console.log(err);
			cb(err);
            conn.close();
        });       
    })
    .catch(function (err) {
		//console.log(err);
		cb(err);
    });
	
}

function fetchDataDBgenesys(keyStr,sqlstring,cb) {
	var conn = new sql.Connection(genSysconfig);
    conn.connect().then(function () {
	    var req = new sql.Request(conn);
	    req.query(sqlstring).then(function (recordset) {
			//console.log(keyStr);
			var obj={};
			obj[keyStr]=recordset;
			var json=JSON.stringify(obj);
			
			//console.log(json);
			cb(null,json);
            conn.close();
        })
        .catch(function (err) {
			//console.log(err);
			cb(err);
            conn.close();
        });       
    })
    .catch(function (err) {
		//console.log(err);
		cb(err);
    });
	
}
