select CONVERT(DATE, startdate, 101) StDate,count(0) Total_Incoming,
				 sum(pending) Pending, sum(markdone) MarkDone, sum(case when FirstReply is null and status =3 then 1 else 0 end) Closed_without_Reply,
				 sum(case when FirstReply is not null and status =3 then 1 else 0 end) Closed_with_Reply,
				 sum(case when FirstReply is  null and status <>3 then 1 else 0 end) Pending_Reply_MarkDone,
				 sum(case when FirstReply is  not null and status <>3 then 1 else 0 end) Pending_MarkDone,
				 sum(case when FirstSLA_Minute between 1 and 240 then 1 else 0 end) TAT_hours_4,
                 sum(case when FirstSLA_Minute  between 241 and 480 then 1 else 0 end) TAT_hours_4_to_8,
				 sum(case when FirstSLA_Minute  between 481 and 720 then 1 else 0 end) TAT_hours_8_to_12,
				 sum(case when FirstSLA_Minute  between 721 and 960 then 1 else 0 end) TAT_hours_12_to_16,
				 sum(case when FirstSLA_Minute  between 961 and 1200 then 1 else 0 end) TAT_hours_16_to_20,
				 sum(case when FirstSLA_Minute  between 1201 and 1440 then 1 else 0 end) TAT_hours_20_to_24,
				 sum(case when FirstSLA_Minute  between 1441 and 2880 then 1 else 0 end) TAT_days_2,
				 sum(case when FirstSLA_Minute  between 2881 and 4320 then 1 else 0 end) TAT_days_3,
				 sum(case when FirstSLA_Minute  between 4321 and 5760 then 1 else 0 end) TAT_days_4,
				 sum(case when FirstSLA_Minute  between 5761 and 7200 then 1 else 0 end) TAT_days_5,
				 sum(case when FirstSLA_Minute >7200 then 1 else 0 end) TAT_days_above_5
from   (select threadid, contactid,SubtypeId, StartDate, 
			   case when FirstReply='1900-01-01 00:00:00.000' then null else FirstReply end FirstReply,
			   isnull(datediff(MINUTE,startdate,case when FirstReply='1900-01-01 00:00:00.000' then null else FirstReply end),0)  FirstSLA_Minute,
			   LastReply Enddate,tot TotalInteractions,case when enddate is null then 'In-Progress' else 'Completed' end IntxnStatus,Category, Status,
			   case when enddate is null then 1 else 0 end Pending,
			   case when enddate is not null then 1 else 0 end MarkDone
		from   (select aa.*,
							   LEAD(aa.FReply, 1,0) OVER (partition by threadid ORDER BY RN) AS FirstReply,
							   LAST_VALUE(enddate) over(partition by threadid order by RN) LastReply
						from   (select aa.*, min(isnull(FReplyON,FReplyOR)) over(partition by threadid) FReply
								 from  (SELECT ContactId,row_number() over(partition by i.threadid order by startdate) RN,  
											   (DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), StartDate)) Startdate,
											   (DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), EndDate)) enddate,
											   status,SubtypeId, threadid,count(0) over(partition by i.threadid) tot,
											   cc.strattribute1 First_Name,cc.strattribute2 Last_Name,cc.strattribute3 email,
											   i.strattribute6 AgentID,subject, text, thecomment, i.strattribute2 Category,
											   case when SubtypeId like 'OutboundNew' then (DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), StartDate)) else null end FReplyON,
											   case when SubtypeId like 'OutboundReply' then (DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), StartDate)) else null end FReplyOR
										  FROM [genesys_ucs_main].[dbo].[Interaction] i,
											   (select distinct threadid thid
												from   [genesys_ucs_main].[dbo].[Interaction]
												where  (DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), StartDate))>='2017-04-03 00:00:00.000'
												and    MediaTypeId='email') ii, [genesys_ucs_main].[dbo].[contact] cc
										where   MediaTypeId='email'
										and     cc.id=i.ContactId
										--and   ContactId='0000WaCCUMYE1BBV'
										--and   ContactID='0001AaCDYTS50E3E'
										and   SubtypeId not in ('OutboundAcknowledgement','InboundReport','InboundNDR')
										and   i.ThreadId=ii.thid)aa) aa) aa
		where  aa.RN =1
		and    SubtypeId like 'Inbound%') aa
group by CONVERT(DATE, startdate, 101)
order by 1 desc