<?php
include_once('session_header.php');
include_once('function.php');

$UserObj;

if(Input::exists('get')){
	
	

    $data =  fetchUsers(Input::get('XID'));
    $jsonObj = json_decode($data,false);
    $output = json_decode($data, true);
    $UserObj=$output['UserDetails'];

}
else {
	
	

  $data =  fetchUsers();
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
  $UserObj=$output['UserDetails'];

}


?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Astro | WinLead Portal</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />
    <link rel="stylesheet" href="css/footable.core.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">





</head>

<body class="fixed-navbar fixed-sidebar" link="Blue">

    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
        <div class="splash-title">
            <h1>Astro - WinLead Portal</h1>
            <p> Please wait while page is loading....</p>
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>
    <!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->



        <div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>
            </div>


        </div>



        <!-- Main Wrapper -->
        <div id="wrapper">

          <div class="row" style="margin-left: 0px;">

                  <div class="panel panel-default" style="height: 90vh;">
                      <div class="panel-heading">
                          <h3 class="panel-title">User Details</h3>
                      </div>


                      <div class="table-responsive" style="height: 80%; width: 100%">
                          <table class="table table-hover" style="max-width: 100%;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                              <thead>
                                  <tr>
                                      <?php createHeaderJSON($UserObj);  ?>
                                  </tr>
                              </thead>
                              <tbody>
                                      <?php createRowsJSON($UserObj); ?>
                              </tbody>
                          </table>
                      </div>

                </div>


          </div>

        </div>

        <!-- Vendor scripts -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <script src="js/index.js"></script>
        <script src="js/metisMenu.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script src="js/jquery.peity.min.js"></script>
        <script src="js/index.js"></script>

        <!-- App scripts -->
        <script src="js/homer.js"></script>
        <script src="js/footable.all.min.js"></script>
        <script src = "js/footable.paginate.js"></script>
        <script src="js/remodal.min.js"></script>

        <script>
        $(function(){
            $('.footable').footable();
        });
        </script>




</body>

</html>
