<?php
include_once('config/init.php');
$done=false;
if(Session::exists(Config::get('session/session_name'))){

        // nothing to do
        if(Input::exists()){
          $db = DB::getInstance();
          $db->update('users',
            array('username'=>Input::get('username')),
            array(
              'password'=> md5(Input::get('password')),
              'change_pass'=> '0'
            )
          );
          if($db->error()){
            echo "Error occurred in changing password" ;
          }else{
            $done = true;
			Redirect::to('login.php');
          }
        }
}else{
    Redirect::to('login.php');
}

$error = false;

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>O2A Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<head>



<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="css/login.css" rel="stylesheet" type="text/css"/>

<style>
#logoleft{
  margin-top: 50px;
}
</style>
</head>
<body>


  <div class="login_page">

    <?php if($error){
    echo '<div class="alert alert-info">
      <button class="close" data-close="alert"></button>
      <span>
      Access Denied !. Please contact IT helpdesk. </span>
    </div>'; } ?>
  	<!-- BEGIN LOGIN FORM -->
  	<div class="form">
      <div class="alert alert-danger useclass hidden">
        <span> Please change your password before login.</span>
      </div>
      <h2>Update Password</h2>

    <form class="login-form" action="" method="post">
  		<div class="alert alert-danger userhidden hidden">
        <span> Username / Password is required</span>
  		</div>

       <div class="input-group input-group-lg">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input readonly value = <?php echo Session::get(Config::get('session/session_name')); ?> type="text" class="form-control" placeholder="Astro user name" id ="username" name="username"/>
        </div>


        <div class="input-group input-group-lg">
          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
          <input type="password" class="form-control" placeholder="New Password" id="password" name="password"/>
        </div>

        <div class="input-group input-group-lg">
          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
          <input type="password" class="form-control" placeholder="Repeat new password" id="confirm_password" name="confirm_password"/>
        </div>



  		<div class="form-actions">
  			<button type="submit" class="btn merchenta pull-right" id="login">
  			Change Password
  			</button>
  		</div>

  	</form>
    <div class="col-xs-12" id="logoleft">
        <span>
           <img src="img/astrologo.png" width="100px"  alt="logo" class="logo-default">
        </span>
    </div>
  	<!-- END LOGIN FORM -->
  	<!-- BEGIN FORGOT PASSWORD FORM -->

  	<!-- END REGISTRATION FORM -->
  </div>
  <script src="js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>

<script>
$('#login').click(function(e){
  if($('#username').val()==''){
    $('.userhidden').removeClass('hidden');
    $('#username').focus();
    return false;
  }
  if($('#password').val()==''){
    $('.userhidden').removeClass('hidden');
    $('#password').focus();
    return false;
  }

});



</script>
</body>
</html>
