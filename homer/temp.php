<?php
include_once('session_header.php');
include_once('function.php');


if(Input::exists('get')){
  $rest = "http://172.18.37.201:8080/WinRest/fetchLeadData/".Input::get('XID');
  $data = file_get_contents($rest);
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
}

//$rest = file_get_contents("data/data.json");
//$jsonObj = json_decode($rest,false);



//$rest = "http://172.18.37.201:8080/WinBackRest/fetchwbAPIinfo/" . $_GET['XID'];
//  $data = file_get_contents($rest);

//$output = json_decode($data, true);




?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Astro | WinLead Portal</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />
    <link rel="stylesheet" href="css/footable.core.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">





</head>

<body class="fixed-navbar fixed-sidebar">

    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
      
        </div>
    </div>
    <!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <!-- Header -->

  

        <div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>

            </div>
             
            <?php 
            $class= 'greenbg';
            //$class = $jsonObj->WinBack->AccountInformation[0]->AccStatus=='InActive'?'redbg':'greenbg'; 
            if(!empty($jsonObj->WinLead->{'Lead Information'}[0])) {
                  echo '<div class="name">
                  Outbound Required
                </div>';
              echo '<span class="status '.$class.'">
                         <i class="fa fa-phone">   </i> ' .$jsonObj->WinLead->{'Lead Information'}[0]->{'Outbound call required'}.'
                   </span>';
            }
            ?>
          
            <?php generateNavBar($role,$role2,$displayName); ?>

        </div>

        

        <!-- Main Wrapper -->
        <div id="wrapper">
        
            <div class="content animate-panel">
            <?php 
            if(!empty($jsonObj->WinLead->{'Lead Information'}[0])){
              ?>
              <div class="row">
                  <div class="col-xs-12 col-lg-9 col-md-9">
                      <div class="row">
                      <div class="hpanel highlight">
                        <div class="panel-heading"><i class="fa fa-user">   </i>
                          <? echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}->{'Full Name'}.'<span class="panetitle">  Mobile # : '.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}->{'Mobile #'}.' </span>  | Office # : '.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}->{'Office #'}
                           ;
                          ?>
                      </div>
                      <div class="panel-body">
                          <div class="row>">
                          <?php
                          $class = 'greenbg';
                          //$class= $jsonObj->WinLead->{Lead Information}[0]->CREDIT_CLASS=='HI'?'redbg':'greenbg';
                          echo '<div class="col-md-2 col-xs-6 ">
                                  <div class="top_title '.$class.'"><i class="fa fa-desktop "> </i> Prospect ID</div>
                                  <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'Prospect Id'}.'</div>
                              </div>';


                          $class= 'infobg';
                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"><i class="fa fa-calendar "> </i> Create Date</div>
                            <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'Create Date'}.'</div>
                          </div>';

                          
                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"> <i class="fa fa-briefcase" aria-hidden="true"></i>
            
 '
                              .$jsonObj->WinLead->{'Lead Information'}[0]->{'ID Type'}.'
                            </div>
                            <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'ID#'}.'</div>
                          </div>';


                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"><i class="fa fa-cube" aria-hidden="true"></i>
 Stage
                            </div>
                            <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'Stage'}.'</div>
                          </div>';
                            
                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"> Race : Indian
                            </div>
                            <div class="top_content '.$class.'">Age -34</div>
                          </div>';
                          $ref = '&nbsp;';
                          if(!is_null($jsonObj->WinLead->{'Lead Information'}[0]->{'Reference Number'})){
                            $ref = $jsonObj->WinLead->{'Lead Information'}[0]->{'Reference Number'};
                          }
                           echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"> Reference Number
                            </div>
                            <div class="top_content '.$class.'">'.$ref.'</div>
                          </div>';
                            

                          ?>
                         
                          </div><!--Pannel Row Closed here-->


                     
                    </div><!--panel body-->
                    </div><!--panel-->
                  </div><!--row pannel-->
                    <div class="row">
                      <div class="col-xs-12 col-md-6">
                          <?php
                          $toggle = array('Prodpect ID');
                          $panel_class = "highlight";
                          $showcols = array('Customer Type','Age','Race');
                          $page = 4;
                          $obj = $jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'};
                          generatePanelBlock('Lead Information >  Customer Details ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                          ?>

                    </div>
                      <div class="col-xs-12 col-md-6">
                          <?php
                          $toggle = array('Prospect ID');
                          $panel_class = "highlight";
                          $showcols = array('Order ID','Product Typ','ARPU(BT)','ARPU(AT)','Camp Code');
                          $page = 4;
                          $obj = $jsonObj->WinLead->{'Lead Information'}[0]->{'Order Information'};
                          generatePanelBlock('Lead Information >  Order Information ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                          ?>
                    
                    </div>

                    </div><!--close row-->

                    <?php 
            if(!empty($jsonObj->WinLead->{'Duplicate Lead Information'})){
          ?>
          

                <?php 
                  if(!is_null($jsonObj->WinLead->{'Duplicate Lead Information'})){
                    foreach ($jsonObj->WinLead->{'Duplicate Lead Information'} as $key => $value) {
                     //do nothing # code...
                       echo '<div class="row">';

                        echo '<div class="col-xs-12 col-md-4">';
                          $toggle = array('Prospect Id');
                          $panel_class = "highlight";
                          $showcols = array('Owner','Stage','ID#');
                          $page = 4;
                          $obj = $value;
                          generatePanelBlock('Duplicate Lead  >  Lead Details ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                          echo '</div>';

                      echo '<div class="col-xs-12 col-md-4">';
                          $toggle = array('Prodpect ID');
                          $panel_class = "highlight";
                          $showcols = array('Age','Race');
                          $page = 4;
                          $obj = $value->{'Customer Information'};
                          generatePanelBlock('Duplicate Lead  >  Customer Details ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                         echo '</div>';

                      echo '<div class="col-xs-12 col-md-4">';
                          $toggle = array('Prospect ID');
                          $panel_class = "highlight";
                          $showcols = array('Prospect Id','Order ID','Product Type','Camp Code');
                          $page = 4;
                          $obj = $value->{'Order Information'};
                          generatePanelBlock('Duplicate Lead  >  Order Information ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                      echo '</div>';
                          echo  '</div>';
                      
                      }
                  }
                }
                ?>
              </div><!--closed left section-->

              <div class="col-xs-12 col-md-3">
                      <div class="hpanel rightpannel">
                        <div class="panel-heading">
                              <div class="">
                                    <div class="left">
                                        Recommondation
                                    </div>
                                    <div class="right">
                                        <i class="home">&nbsp</i>
                                    </div>
                              </div>
                          </div>
                          <div class="panel-body ">
                           <table class="table table-striped">
                           <tr>
                           <td>Stage<td><?php echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Recommendation'}->Satge; ?><td>
                           </tr>
                           <tr>
                           <td>Action<td><?php echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Recommendation'}->Action;?><td>
                           </tr>
                           <tr>
                           <td>SubAction<td><?php echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Recommendation'}->{'Sub Action'}; ?><td>
                           </tr>
                           </table>
                          </div>
                      </div>

            
            
                       </div>

         
          <?php }?>
          
               <?php 
                if(!empty($jsonObj->{'Existing Account Information'})){

                ?>


                <div class="col-xs-12 col-md-3">
                <?php 
                $toggle = array('Account ID');
                $panel_class = "highlight";
                $showcols = array('Debt Age','O/S Bal','Due Amt');
                $pagesize = 'data-page-size="5"';
                generatePanelBlock("Existing Account",$pagesize,$jsonObj->WinLead->{'Existing Account Information'},$toggle,$showcols); ?>
                </div>

              </div>

              <?php }?>
                    <!-- Fist Panel goes here -->

                    


                  


                <!---4th Pannel -->



                </div>
                <div class="remodal" data-remodal-id="modal">
                  <button data-remodal-action="close" class="remodal-close"></button>
                  <h1>Interactions</h1>
                  <p>
                   <?php
                      if(empty($jsonObj->WinBack->IntrxnInformation)){
                      //Do nothing
                        }else{
                          $toggle = array('START_TIME');
                          $showcols = array('Title_Type','Media','Direction');

                          $count = count($jsonObj->WinBack->IntrxnInformation);
                          $pagesize = "";
                          if($count>10){
                            $pagesize = 'data-page-size="10"';
                          }


                        generatePanelBlock("",$pagesize,$jsonObj->WinBack->IntrxnInformation,$toggle,$showcols);
                      }

                      ?>
                  </p>
                  <br>
                  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>


                  <div class="remodal" data-remodal-id="pendingOrders">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <h1>Pending Orders</h1>
                      <p>
                       <?php
                        if(!is_null($jsonObj->WinLead->{'Existing Account Information'})){
                        foreach ($jsonObj->WinLead->{'Existing Account Information'} as $key => $value) {
                          if(is_object($value)&&!empty($value)){
                            foreach ($value->{'Pending Order Information'} as $header => $pendingObj) {

                                generatePanelBlock("","20",$pendingObj,$toggle,$showcols);
                            }
                             
                            }
                          }

                      }
                         
                          ?>
                      </p>
                      <br>
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>



            <!-- Right sidebar -->



        </div>

        <!-- Vendor scripts -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <script src="js/index.js"></script>
        <script src="js/metisMenu.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script src="js/jquery.peity.min.js"></script>
        <script src="js/index.js"></script>

        <!-- App scripts -->
        <script src="js/homer.js"></script>
        <script src="js/footable.all.min.js"></script>
        <script src = "js/footable.paginate.js"></script>
        <script src="js/remodal.min.js"></script>

        <script>
        $(function(){
            $('.footable').footable();
        });


        </script>
          <script>
        $('td').on("click",function(evt){
            val = $(this).text();
            //alert(val);
            $('.active').each(function(){
              $(this).removeClass('active');
              $(this).removeClass('footable-odd');
              $(this).removeClass('footable-even');
            });
            $('[data-id]').each(function(){
              if(val==$(this).text()){
                  $(this).parent().addClass('active');
              }
            });
        });

        $(function(){
           $(window).resize(function(){
            console.log($('.highlight >.panel-body').height());
           });
        });
    </script>




</body>

</html>
