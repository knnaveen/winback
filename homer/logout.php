<?php
include_once('config/init.php');
$db = DB::getInstance();
$db->delete('table_user_sessions',array(
    array('username','=',Session::get(Config::get('session/session_name'))),
    array('agent','=',$_SERVER['HTTP_USER_AGENT'])
));
if(!$db->error()){
    $logout = true;
    Session::delete(Config::get('session/session_name'));
    Session::delete(Config::get('session/session_role'));
    Session::delete(Config::get('session/displayName'));
	 Session::delete(Config::get('session/userRole'));
    Session::delete(Config::get('session/session_role2'));
    Cookie::delete(Config::get('logged/cookie_name'));
    session_destroy();
    Redirect::to('login.php');
}
?>
