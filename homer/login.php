<?php
include_once('config/init.php');
$error = false;
//setcookie(Config::get('logged/cookie_name'),'n',time()+(Config::get('logged/cookie_expiry')),'/');
if(Input::exists()){

    
	
	$user= new User();
    $login = $user->login(Input::get('username'),Input::get('password'),Input::get('role'));

    if($login){

      if(Session::get(Config::get('session/session_role'))=="lead"){
        Redirect::to('lead.php');
      }else{
         Redirect::to('index.php');
      }
  }else{
    $error = true;
  }
  
  
  
  
  
  
  
}




?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Astro - WinLead Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<head>



<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="css/login.css" rel="stylesheet" type="text/css"/>

<style>
#logoleft{
  margin-top: 50px;
}
</style>
</head>
<body>


<div class="login-page">

  	<!-- BEGIN LOGIN FORM -->
  	<div class="form">
      <h2>WinLead Portal | Login</h2>
      <?php if($error){
      echo '<div class="alert alert-info">

        <span>
        Access Denied ! <br> Please contact IT helpdesk. </span>
      </div>'; } ?>
    <form class="login-form" action="" method="post">

       <div class="input-group input-group-lg">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input type="text" class="form-control" placeholder="Astro user name" id ="username" name="username"/>
        </div>

        <div class="input-group input-group-lg">
          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
          <input type="password" class="form-control" placeholder="Password" id="password" name="password"/>
        </div>
        <div class="input-group input-group-lg">
          <span class="radio_group" >
            <input type="radio" id="t-option" value="winback" checked="checked" name="role">
            <label for="t-option">Winback</label>
          </span>
          <span class="radio_group" >
            <input type="radio" id="t-option" value="lead" name="role">
            <label for="t-option">Lead</label>
          </span>
        </div>


  		<div class="form-actions">
  			<button type="submit" class="btn merchenta pull-right" id="login">
  			Login
  			</button>
  		</div>

  	</form>
    <div class="col-xs-12" id="logoleft">
        <span>
           <img src="img/astrologo.png" width="100px"  alt="logo" class="logo-default">
        </span>
    </div>
  	<!-- END LOGIN FORM -->
  	<!-- BEGIN FORGOT PASSWORD FORM -->

  	<!-- END REGISTRATION FORM -->
  </div>
  <script src="js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>

<script>
$('#login').click(function(e){
  if($('#username').val()==''){
    $('.userhidden').removeClass('hidden');
    $('#username').focus();
    return false;
  }
  if($('#password').val()==''){
    $('.userhidden').removeClass('hidden');
    $('#password').focus();
    return false;
  }

});



</script>
</body>
</html>
