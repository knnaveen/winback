<?php
ini_set("date.timezone", "Asia/Kuala_Lumpur");

include_once('session_header.php');
include_once('function.php');


if(Input::exists('get')){

  $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
	
	
  //$data = file_get_contents("data/data.json");
  
  $name = Input::get('XID');
  
  if(is_null($name) or $name == '')
	  $name = 'OPENS';
  
  error_log($name);
  $rest = "http://172.18.37.201:8080/WinRest/fetchCampData/".$name;
  
  error_log($rest);
  $data = file_get_contents($rest);
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
  $recoclass;
  
    $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	

	$time_elapsed_secs = microtime(true) - $start;
	error_log ('LEAD - Host: '.$_SERVER['REMOTE_ADDR'].' '.Input::get('XID').' '.$total_time,0);
}
else
{
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
	
	
  //$data = file_get_contents("data/data.json");
  
 
	  $name = 'OPENS';
  
  error_log($name);
  $rest = "http://172.18.37.201:8080/WinRest/fetchCampData/".$name;
  
  error_log($rest);
  $data = file_get_contents($rest);
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
  $recoclass;
  
    $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	

	$time_elapsed_secs = microtime(true) - $start;
	error_log ('LEAD - Host: '.$_SERVER['REMOTE_ADDR'].' '.Input::get('XID').' '.$total_time,0);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	
    <!-- Page title -->
    <title>Campaign Management | WinLead Portal</title>

    <!-- Bootstrap core CSS -->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" rel="stylesheet">

	<!-- Prism -->
	<link href="css/prism.css" rel="stylesheet">

	<!-- FooTable Bootstrap CSS -->
	<link href="compiled/footable.bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/docs.css" rel="stylesheet">

<!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />

    <link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />
	
	<link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">

    
</head>

<body class="docs">
	
<div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>

            </div>
             
           
            <?php generateNavBarNew($userRole,$role,$role2,$displayName,"Search Campaign"); ?>

 </div>

        <!-- Main Wrapper -->
        <div class="content animate-panel">
        
            <div class="docs-section">
            
          
               <?php 
                if(!empty($jsonObj->WinLeadCamp->{'Campaign Information'})){
				?>
				
				
                <div class="example">
                
				<?php 
              
                $panel_class = "highlight";
				
			    $showcols = array("CAMPAIGN" ,
								"PROMOCODE",
								"ACTIVE",
								"VTSTS",
								"MINSUB",
                                  "MINPKG",
                                  "EFFDATE",
                                  "ENDDATE",
								  "PRODUCT"
                                  );
				 $toggle = array("PAYMENT" ,
								"CUSTOMER",
								"NATIONALITY",
								"RACE",
								"REGION",
                                  "REMARKS"
                                  );

                $pagesize = 'data-page-size="50"';
                generateCampBlockNew("Existing CAMPAIGN",$pagesize,$jsonObj->WinLeadCamp->{'Campaign Information'},$toggle,$showcols); 
				?>
				
				<div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
				
				<style scoped>
					/* provides a red astrix to denote required fields */
					.form-group.required .control-label:after {
						content:"*";
						color:red;
						margin-left: 4px;
					}
				</style>
				
				<div class="modal-dialog" role="document">
					<form class="modal-content form-horizontal" id="editor">
						<div class="modal-footer">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4  id="editor-title">Add Campaign</h4>
						</div>
						<div class="modal-body">
							<input type="number" id="id" name="id" class="hidden"/>
							
							<div class="form-group required">
								<label for="PROMOCODE" class="col-sm-3 control-label">Promo Code</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="PROMOCODE" name="PROMOCODE" placeholder="Promo Code" required>
								</div>
							</div>
							
							<div class="form-group required">
								<label for="CAMPAIGN" class="col-sm-3 control-label">Campaign</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="CAMPAIGN" name="CAMPAIGN" placeholder="Campaign" required>
								</div>
							</div>
							
							<div class="form-group">
								<label for="MINSUB" class="col-sm-3 control-label">Min Sub Fee</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="MINSUB" name="MINSUB" placeholder="Min Sub Fee">
								</div>
							</div>
							<div class="form-group">
								<label for="MINPKG" class="col-sm-3 control-label">Min Sub PKG</label>
								<div class="col-sm-9">
								<select class="form-control"  id="MINPKG" name="MINPKG">
									<option type="text" class="form-control"  value="" ></option>
									<option type="text" class="form-control"  value="Chinese Family Extra" >Chinese Family Extra</option>
									<option type="text" class="form-control"  value="Family" >Family</option>
									<option type="text" class="form-control"  value="Family HD" >Family HD</option>
									<option type="text" class="form-control"  value="Family Lite" >Family Lite</option>
									<option type="text" class="form-control"  value="Family Special Pack" >Family Special Pack</option>
									<option type="text" class="form-control"   value="FamilyLite">FamilyLite</option>
									<option type="text" class="form-control"   value="Super Pack">Super Pack</option>
									<option type="text" class="form-control"  value="Super Pack 3" >Super Pack 3</option>
									<option type="text" class="form-control"  value="Super Pack Plus" >Super Pack Plus</option>
									<option type="text" class="form-control"  value="Value Pack" >Value Pack</option>
								</select>
								</div>
							</div>
							
						
						<div class="form-group">
								<label for="PAYMENT_METHOD" class="col-sm-3 control-label">PAYMENT METHOD</label>
								<div class="col-sm-9">
								<select class="form-control"  id="PAYMENT" name="PAYMENT_METHOD">	
									<option type="text" class="form-control" value=""  ></option>	
									<option type="text" class="form-control" value="Cash"  >Cash</option>
									<option type="text" class="form-control" value="Direct Debit"  >Direct Debit</option>
									<option type="text" class="form-control" value="Credit Card" >Credit Card</option>					
								</select>
								</div>
						</div>
						
						<div class="form-group">
								<label for="REGION" class="col-sm-3 control-label">REGION</label>
								<div class="col-sm-9">
								<select class="form-control"  id="REGION" name="REGION">	
									<option type="text" class="form-control" value=""  ></option>	
									<option type="text" class="form-control" value="PLS,KED,PRK,PNG"  >PLS,KED,PRK,PNG</option>
									</select>
								</div>
						</div>
						
						<div class="form-group">
								<label for="NATIONALITY_ELIGIBILITY" class="col-sm-3 control-label">NATIONALITY ELIGIBILITY</label>
								<div class="col-sm-9">
								<select class="form-control"  id="NATIONALITY_ELIGIBILITY" name="NATIONALITY_ELIGIBILITY">	
									<option type="text" class="form-control"  value="" ></option>
									<option type="text" class="form-control"  value="Passport" >Passport</option>
									<option type="text" class="form-control" value="NRIC" >NRIC</option>					
								</select>
								</div>
							</div>

						
						<div class="form-group">
								<label for="CUSTOMER_ELIGIBILITY" class="col-sm-3 control-label">CUSTOMER ELIGIBILITY</label>
								<div class="col-sm-9">
								<select class="form-control"  id="CUSTOMER_ELIGIBILITY" name="CUSTOMER_ELIGIBILITY">	
									<option type="text" class="form-control"  value="" ></option>	
									<option type="text" class="form-control"  value="Brownfield Customer" >Brownfield Customer</option>
									<option type="text" class="form-control"  value="Greenfield customers">Greenfield customers</option>					
								</select>
								</div>
							</div>
						
						<div class="form-group">
								<label for="PRODUCT" class="col-sm-3 control-label">Product Type</label>
								<div class="col-sm-9">
								<select class="form-control"  id="PRODUCT" name="PRODUCT">
									<option type="text" class="form-control" value="BYOND"  >BYOND</option>
									<option type="text" class="form-control"  value="BYOND (MINI MULTIROOM)" >BYOND (MINI MULTIROOM)</option>
									<option type="text" class="form-control"  value="BYOND (MULTIROOM)" >BYOND (MULTIROOM)</option>
									<option type="text" class="form-control"  value="BYOND_PVR" >BYOND_PVR</option>
									<option type="text" class="form-control"  value="NJOI" >NJOI</option>
									<option type="text" class="form-control"  value="NJOI TO DTH" >NJOI TO DTH</option>
									<option type="text" class="form-control"  value="NJOI TO DTH (PVR)" >NJOI TO DTH (PVR)</option>
									<option type="text" class="form-control"  value="NJOIUpgradeRemarketing" >NJOIUpgradeRemarketing</option>
								</select>
								</div>
							</div>
											
						<div class="form-group">
								<label for="EFFDATE" class="col-sm-3 control-label">Effective Date</label>
								<div class="col-sm-9">
									<input type="date" class="form-control" id="EFFDATE" name="EFFDATE" placeholder="Effective Date">
								</div>
							</div>
							<div class="form-group">
								<label for="ENDDATE" class="col-sm-3 control-label">End Date</label>
								<div class="col-sm-9">
									<input type="date" class="form-control" id="ENDDATE" name="ENDDATE" placeholder="End Date">
								</div>
							</div>
							
							<div class="form-group">
								<label for="REMARKS" class="col-sm-3 control-label">ENDDATE REMARKS</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="REMARKS" name="REMARKS" placeholder="ENDDATE REMARKS" >
								</div>
							</div>
							
							<div class="form-group required">
								<label for="ACTIVE" class="col-sm-3 control-label">ACTIVE</label>
								<div class="col-sm-9">
								<select class="form-control"  id="ACTIVE" name="ACTIVE">	
									<option type="text" class="form-control"  value="ACTIVE" >ACTIVE</option>
									<option type="text" class="form-control" value="EXPIRED" >EXPIRED</option>					
								</select>
								</div>
							</div>
							<div class="form-group required">
								<label for="ACTIVE" class="col-sm-3 control-label">RACE ELIGIBILITY</label>
								<div class="col-sm-9">
								<select class="form-control"  id="RACE" name="RACE">	
									<option type="text" class="form-control"  value="" ></option>
									<option type="text" class="form-control" value="MALAY" >MALAY</option>					
								</select>
								</div>
							</div>
							<div class="form-group required">
								<label for="VTSTS" class="col-sm-3 control-label">VT Status</label>
								<div class="col-sm-9">
								<select class="form-control"  id="VTSTS" name="VTSTS">	
									<option type="text" class="form-control" value="ACTIVE"  >ACTIVE</option>
									<option type="text" class="form-control"  value="EXPIRED" >EXPIRED</option>					
								</select>
								</div>
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Save changes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>

				</div>
				
              </div>

	
              <?php }
              ?>

    </div>
      


<!-- Placed at the end of the document so the pages load faster -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
	
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/prism.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Add in any FooTable dependencies we may need -->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<!-- Add in FooTable itself -->
<script src="compiled/footable.js"></script>
<!-- Initialize FooTable -->

<script>

$('#CUSTOMER_ELIGIBILITY').editableSelect();
$('#NATIONALITY_ELIGIBILITY').editableSelect();
$('#REGION').editableSelect();
$('#MINPKG').editableSelect();
$('#PAYMENT').editableSelect();
$('#RACE').editableSelect();

	jQuery(function($){
		var $modal = $('#editor-modal'),
			$editor = $('#editor'),
			$editorTitle = $('#editor-title'),
			ft = FooTable.init('#editing-example', {
				editing: {
					enabled: true,
					addRow: function(){
						$modal.removeData('row');
						$editor[0].reset();
						$editorTitle.text('Add a new Campaign....');
						$modal.modal('show');
					},
					editRow: function(row){
					//	confirm('in the edit');
						var values = row.val();
						$editor.find('#id').val(values.ID);
						$editor.find('#CAMPAIGN').val(values.CAMPAIGN);
						$editor.find('#ACTIVE').val(values.ACTIVE);
						$editor.find('#VTSTS').val(values.VTSTS);
						$editor.find('#PROMOCODE').val(values.PROMOCODE);
						$editor.find('#MINSUB').val(values.MINSUB);
						$editor.find('#MINPKG').val(values.MINPKG);
						$editor.find('#PRODUCT').val(values.PRODUCT);
						$editor.find('#CUSTOMER_ELIGIBILITY').val(values.CUSTOMER);
						$editor.find('#NATIONALITY_ELIGIBILITY').val(values.NATIONALITY);
						$editor.find('#EFFDATE').val(values.EFFDATE);
						$editor.find('#ENDDATE').val(values.ENDDATE);
						$editor.find('#PAYMENT').val(values.PAYMENT);
						$editor.find('#REMARKS').val(values.REMARKS);
						$editor.find('#RACE').val(values.RACE);
						$editor.find('#REGION').val(values.REGION);
						
						//console.log(values);
						$modal.data('row', row);
						$editorTitle.text('Edit row #' + values.ID);
						$modal.modal('show');
					},
					deleteRow: function(row){
						if (confirm('You Are Not Allowed to Delete Row')){
							//row.delete();
						}
					}
				}
			}),
			uid = 10;

		$editor.on('submit', function(e){
			if (this.checkValidity && !this.checkValidity()) return;
			e.preventDefault();
			
			var row = $modal.data('row'),
				values = ({
					id : $editor.find('#id').val(),
					CAMPAIGN : $editor.find('#CAMPAIGN').val(),
					PROMOCODE : $editor.find('#PROMOCODE').val(),
					MINPKG : $editor.find('#MINPKG').val(),
					MINSUB : $editor.find('#MINSUB').val(),
					EFFDATE : $editor.find('#EFFDATE').val(),
					ENDDATE : $editor.find('#ENDDATE').val(),
					PRODUCT : $editor.find('#PRODUCT').val(),
					REMARKS : $editor.find('#REMARKS').val(),
					ACTIVE : $editor.find('#ACTIVE').val(),
					VTSTS : $editor.find('#VTSTS').val(),
					CUSTOMER : $editor.find('#CUSTOMER_ELIGIBILITY').val(),
					PAYMENT : $editor.find('#PAYMENT').val(),
					NATIONALITY : $editor.find('#NATIONALITY_ELIGIBILITY').val(),
					REGION : $editor.find('#REGION').val(),
					RACE : $editor.find('#RACE').val()
				});

			if (row instanceof FooTable.Row){
				console.log(JSON.stringify(values));
				UpdateData(JSON.stringify(values));	
				row.val(values);
			} else {
				values.id = uid++;
				ft.rows.add(values);
				console.log(JSON.stringify(values));
				console.log(JSON.stringify(values));
				PostData(JSON.stringify(values));
			}
			
			$modal.modal('hide');
		});
	});
	function PostData(values){
            //var table;
            var url;
              url = "http://172.18.37.201:8080/WinRest/CampData";
           

			var xhttp = new XMLHttpRequest();
			xhttp.open("POST", url, true);
			xhttp.setRequestHeader("Content-type", "application/json");
			xhttp.send(values);
			var response = xhttp.responseText;   
        }
function UpdateData(values){
            //var table;
            var url;
              url = "http://172.18.37.201:8080/WinRest/CampData";
          
			console.log(values);
			var xhttp = new XMLHttpRequest();
			xhttp.open("PUT", url, true);
			xhttp.setRequestHeader("Content-type", "application/json");
			xhttp.send(values);
			var response = xhttp.responseText;   
        }
</script>
</body>

<?php
	flush();
    ob_flush(); 
?>

</html>
