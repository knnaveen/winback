<?php
ini_set("date.timezone", "Asia/Kuala_Lumpur");

include_once('session_header.php');
include_once('function.php');


if(Input::exists('get')){

  $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
	
	
  //$data = file_get_contents("data/data.json");
  $rest = "http://172.18.37.201:8080/WinRest/fetchLeadData/".Input::get('XID');
  $data = file_get_contents($rest);
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
  $recoclass;
  
    $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	

	$time_elapsed_secs = microtime(true) - $start;
	error_log ('LEAD - Host: '.$_SERVER['REMOTE_ADDR'].' '.Input::get('XID').' '.$total_time,0);
}


//$jsonObj = json_decode($rest,false);



//$rest = "http://172.18.37.201:8080/WinBackRest/fetchwbAPIinfo/" . $_GET['XID'];
//  $data = file_get_contents($rest);

//$output = json_decode($data, true);




?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Astro | WinLead Portal</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />
    <link rel="stylesheet" href="css/footable.core.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">





</head>

<body class="fixed-navbar fixed-sidebar">

    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
      
        </div>
    </div>
    <!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <!-- Header -->

  

        <div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>

            </div>
             
            <?php 
            if(!empty($jsonObj->WinLead->{'Lead Information'}[0])){
                  if($jsonObj->WinLead->{'Lead Information'}[0]->{'rag'}=='R'){
                     $class= 'redbg';
                     $recoclass = $class;
                  }elseif($jsonObj->WinLead->{'Lead Information'}[0]->{'rag'}=='G'){
                    $class= 'greenbg';
                    $recoclass = $class;
                  }else{
                    $class= 'amberbg';
                    $recoclass = $class;
                  }
            }
            

            //$class = $jsonObj->WinBack->AccountInformation[0]->AccStatus=='InActive'?'redbg':'greenbg'; 
            if(!empty($jsonObj->WinLead->{'Lead Information'}[0])) {
                 echo '<div class="status"></div>';
              echo '<span class="status '.$class.'">
                         <i class="fa fa-phone">   </i> ' .$jsonObj->WinLead->{'Lead Information'}[0]->{'Outbound call required'}.'
                   </span>
                   <span class="status">
                      '
                        .$jsonObj->WinLead->{'Lead Information'}[0]->{'cbdnote'}.
                      '
                   </span>
                   ';

            }
            ?>
          
            <?php generateNavBar($userRole,$role,$role2,$displayName); ?>

        </div>

        

        <!-- Main Wrapper -->
        <div id="wrapper">
        
            <div class="content animate-panel">
            <?php 
            if(!empty($jsonObj->WinLead->{'Lead Information'}[0])){
              ?>
              <div class="row">
                  <div class="col-xs-12 col-lg-9 col-md-9">
                      <div class="hpanel highlight">
                        <div class="panel-heading">
                          <?php 

                           echo'
                            <span id="fullname" class="topspans"><i class="fa fa-user">   </i> '.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}[0]->{'Full Name'}.
                            '</span>
                            <span class="topspans">  <i class="fa fa-mobile" aria-hidden="true"></i>
# : '.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}[0]->{'Mobile #'}.
                            ' </span>
                             <span class="topspans"> <i class="fa fa-phone-square" aria-hidden="true"></i>
 # : '.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}[0]->{'Office #'}.
                             '</span><span class="topspans"><i class="fa fa-envelope" aria-hidden="true"></i>
 # : '.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}[0]->{'E_MAIL'}.
                             '</span>';

                           ;
                          ?>
                      </div>
                      <div class="panel-body">
                          <div class="row>">
                          <?php
                          $class = 'greenbg';
                          //$class= $jsonObj->WinLead->{Lead Information}[0]->CREDIT_CLASS=='HI'?'redbg':'greenbg';
                          echo '<div class="col-md-2 col-xs-6 ">
                                  <div class="top_title '.$class.'"><i class="fa fa-desktop "> </i> Prospect ID</div>
                                  <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'Prospect Id'}.'</div>
                              </div>';


                          $class= 'infobg';
                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"><i class="fa fa-calendar "> </i> Create Date</div>
                            <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'Create Date'}.'</div>
                          </div>';

                          if($jsonObj->WinLead->{'Lead Information'}[0]->{'ID Type'}=='NRIC'){
                            $class= 'greenbg';
                          }else{
                            $class= "purplebg";
                          }
                          
                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"> <i class="fa fa-user-secret" aria-hidden="true"></i> '
                            .$jsonObj->WinLead->{'Lead Information'}[0]->{'ID Type'}.'
                            </div>
                            <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'ID#'}.'</div>
                          </div>';

                            $class= 'infobg';

                            switch ($jsonObj->WinLead->{'Lead Information'}[0]->{'Stage'}) {
                              case 'VT-UTC1':
                                # code...
                                $class = "amberbg";
                                break;
                              case 'VT-UTC2':
                                # code...
                                $class = "amberbg";
                                break;
                              case 'VT-ICF1':
                                # code...
                                $class = "amberbg";
                                break;
                              case 'VT-Lost':
                                # code...
                                $class = "redbg";
                                break;
                              default:
                                # code...
                                $class = "greybg";
                                break;
                            }
                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"><i class="fa fa-cube" aria-hidden="true"></i>
 Stage
                            </div>
                            <div class="top_content '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'Stage'}.'</div>
                          </div>';
                          $class= 'infobg';
                            
                          echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'">'.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}[0]->{'PaymentType'}.'
                            </div>
                            <div class="top_content '.$class.'">BillFreq '.$jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Information'}[0]->{'Bill Freq'}.'</div>
                          </div>';
                          $ref = '&nbsp;';
                          if(!is_null($jsonObj->WinLead->{'Lead Information'}[0]->{'Reference Number'})){
                            $ref = $jsonObj->WinLead->{'Lead Information'}[0]->{'Reference Number'};
                          }
                           echo '<div class="col-md-2 col-xs-6 ">
                            <div class="top_title '.$class.'"> <i class="fa fa-info" aria-hidden="true"></i> Ref Number
                            </div>
                            <div class="top_content '.$class.'">'.$ref.'</div>
                          </div>';
                            

                          ?>
                         
                          </div><!--Pannel Row Closed here-->


                     
                    </div><!--panel body-->
                    </div><!--panel-->

                  <!-- This is new change -->

                  <!--Move Recommodation here -->

                </div>
                 <div class="col-xs-12 col-md-3">
                      <div class="hpanel rightpannel">
                        <div class="panel-heading">
                              <div class="">
                                    <div class="left">
                                        <i class="fa fa-map-pin" aria-hidden="true"></i>
 Recommendation - Rule #  <?php echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Recommendation'}->{'Rule No'}; ?>
                                    </div>
                                    <div class="right">
                                        <i class="home">&nbsp</i>
                                    </div>
                              </div>
                          </div>
                          <div class="panel-body>">
                           <table class="table">
                           <tr class="<?php echo $recoclass ?>" >
                           <td>Stage<td><?php echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Recommendation'}->Stage; ?><td>
                           </tr>
                           <tr class="<?php echo $recoclass ?>">
                           <td>Action<td><?php echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Recommendation'}->Action;?><td>
                           </tr>
                           <tr class="<?php echo $recoclass ?>">
                           <td>SubAction<td><?php echo $jsonObj->WinLead->{'Lead Information'}[0]->{'Recommendation'}->{'Sub Action'}; ?><td>
                           </tr>
                           </table>
                          </div>
                      </div>

            
            
                       </div>

              </div>
                    <div class="row">
                      <div class="col-xs-12 col-md-12">
                          <?php
                          $toggle = array();
                          $panel_class = "highlight";
                          $showcols = array('Full Name','Customer Type','Sub Type','Race','State','Service Address','DOB','Card Type','Other ID Type',"Age");
                          $page = 4;
                          $obj = $jsonObj->WinLead->{'Lead Information'}[0]->{'Customer Details'};
                          generatePanelBlock('Lead Information >  Customer Details ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                          ?>

                    </div>
                      <div class="col-xs-12 col-md-12">
                          <?php
                          $toggle = array('Prospect ID');
                          $panel_class = "highlight";
                          $showcols =  array("Order ID",
                            "Market Promo",
                            "Camp Code",
                            "Min Subscription Fee",
                            "ARPU(BT)",
                            "ARPU(AT)",
                            "SalesPerson Code",
                            "DMT",
                            "SMC",
                            "DMT",
                            "Status",
							"Product Type",
							"Packages"
                            );
                          $page = 4;
                          $obj = $jsonObj->WinLead->{'Lead Information'}[0]->{'Order Information'};
                          generatePanelBlock('Lead Information >  Order Information ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                          ?>
                    
                    </div>

                    </div><!--close row-->
                    <div class="row">
                      <div class="col-lg-12">

                    <?php 
            if(!empty($jsonObj->WinLead->{'Duplicate Lead Information'})){
          ?>
          

                <?php 
                  if(!is_null($jsonObj->WinLead->{'Duplicate Lead Information'})){
                    foreach ($jsonObj->WinLead->{'Duplicate Lead Information'} as $key => $value) {
                     //do nothing # code...
                       echo '<div class="row">';

                        echo '<div class="col-xs-12 col-md-4">';
                          $toggle = array('Prospect Id');
                          $panel_class = "highlight";
                          $showcols = array('Owner','Stage','ID#');
                          $page = 4;
                          $obj = $value;
                          generatePanelBlock('Duplicate Lead  >  Lead Details ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                          echo '</div>';

                      echo '<div class="col-xs-12 col-md-4">';
                          $toggle = array('Race');
                          $panel_class = "highlight";
                          $showcols = array('Age');
                          $page = 4;
                          $obj = $value->{'Customer Information'};
                          generatePanelBlock('Duplicate Lead  >  Customer Details ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                         echo '</div>';

                      echo '<div class="col-xs-12 col-md-4">';
                          $toggle = array('Prospect ID');
                          $panel_class = "highlight";
                          $showcols = array('Prospect Id','Order ID','Product Type','Camp Code');
                          $page = 4;
                          $obj = $value->{'Order Information'};
                          generatePanelBlock('Duplicate Lead  >  Order Information ', 
                            $page,
                            $obj,
                            $toggle,
                            $showcols,
                            $panel_class
                          );
                      echo '</div>';
                          echo  '</div>';
                      
                      }
                  }
                }
                ?>
              </div><!--closed left section-->

             


         
          <?php }?>
          
               <?php 
                if(!empty($jsonObj->WinLead->{'Existing Account Information'})){

                ?>


                <div class="col-xs-12 col-md-12">
                <?php 
                $toggle = array();
                $panel_class = "highlight";
                $showcols = array("Account ID" ,"Acc Type",
                                  "Full Name",
                                  "Debt Age",
                                  "Due Amt",
                                  "O\/S Bal",
                                  "Home Phone",
                                  "Detected BY",
                                  "Mobile #",
                                  "ID Type",
                                  "ID#",
                                  "AccountSts"
                                  );

                $pagesize = 'data-page-size="5"';
                generatePanelBlock("Existing Account",$pagesize,$jsonObj->WinLead->{'Existing Account Information'},$toggle,$showcols); ?>
                </div>

              </div>


              <?php }
              ?>
                    <!-- Fist Panel goes here -->

                    


                  


                <!---4th Pannel -->



                </div>
                <div class="remodal" data-remodal-id="modal">
                  <button data-remodal-action="close" class="remodal-close"></button>
                  <h1>Interactions</h1>
                  <p>
                   <?php
                      if(empty($jsonObj->WinBack->IntrxnInformation)){
                      //Do nothing
                        }else{
                          $toggle = array('START_TIME');
                          $showcols = array('Title_Type','Media','Direction');

                          $count = count($jsonObj->WinBack->IntrxnInformation);
                          $pagesize = "";
                          if($count>10){
                            $pagesize = 'data-page-size="10"';
                          }


                        generatePanelBlock("",$pagesize,$jsonObj->WinBack->IntrxnInformation,$toggle,$showcols);
                      }

                      ?>
                  </p>
                  <br>
                  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>




                  <div class="remodal" data-remodal-id="pendingOrders">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <h1>Pending Orders</h1>
                      <p>
                       <?php
                        if(!is_null($jsonObj->WinLead->{'Existing Account Information'})){
                        foreach ($jsonObj->WinLead->{'Existing Account Information'} as $key => $value) {
                          if(is_object($value)&&!empty($value)){
                            foreach ($value->{'Pending Order Information'} as $header => $pendingObj) {

                                generatePanelBlock("","20",$pendingObj,$toggle,$showcols);
                            }
                             
                            }
                          }

                      }
                         
                          ?>
                      </p>
                      <br>
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>


                  <!--Interaction for the account -->
                    <div class="remodal" data-remodal-id="intr1">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <h1>Interactions</h1>
                        <p id="intr1">
                        </p>
                      <br>
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>


                  <!--Interaction for the account -->
                    <div class="remodal" data-remodal-id="smc1">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <h1>Smart Card Details</h1>
                        <p id="smc1">
                        </p>
                      <br>
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>

				 <!--Interaction for the account -->
                    <div class="remodal" data-remodal-id="myalert">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      <h1>Alerts</h1>
                        <p id="">
						<div class ="alert alert-danger" >
							Important: Application contain “Nielsen” under personal or commercial submission, please check account flashes at 0943100896 for relevant contact no., email, PIC. Obtain VT HOD approval if any
						</div>
						<div class="alert alert-danger">
						Escalate to Elite if submission from 7BF, 7BK, 7FN, 84Y, 85S, 8S3, DNM, GM7, GMF, GMR, HUA, JL6, KS3, O0102011,P0102041,P0102047, P0102069,P0102238, P0102269
						</div>
                        </p>
                      <br>
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>




            <!-- Right sidebar -->



        </div>

        <!-- Vendor scripts -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <script src="js/index.js"></script>
        <script src="js/metisMenu.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script src="js/jquery.peity.min.js"></script>
        <script src="js/index.js"></script>

        <!-- App scripts -->
        <script src="js/homer.js"></script>
        <script src="js/footable.all.min.js"></script>
        <script src = "js/footable.paginate.js"></script>
        <script src="js/remodal.min.js"></script>

        <script>
        $(function(){
            $('.footable').footable();
        });

        // Calling Interactions
        $(function(){
          $('.interaction').click(function(){
              //$('#intr').html($(this).data('acts'));
              var account = $(this).data('acts');
              getAjaxData('int',account,'intr1');
             
          });
          $('.smc').click(function(){
              var account = $(this).data('acts');
             getAjaxData('smc',account,'smc1');
          });
        });

        function getAjaxData(type,account_no,modalId){
            //var table;
            var url;
            if(type=="int"){
              url = "http://172.18.37.201:8080/WinBackRest/fetchintrxninfo/"+account_no;
              //url = "data/int.json";
            }else{
              url = "http://172.18.37.201:8080/WinBackRest/fetchsmcinfo/"+account_no;
              //url = "data/smc.json";
            }
            console.log(url);

        
             $.getJSON(url).done(function(data){
                  var tableMain = '<table class="table footable table-bordered table-striped">';
                  var tableHeader ="<thead>" ;
                  var tableBody = "<tbody>";
                  var count= 0; //take header from one object only
                  var jsonKey ;
                  if(type=='int'){
                    jsonKey = data.IntrxnInformation;
                  }else{
                    jsonKey = data.SMCInformation;
                  }
                  $.each(jsonKey, function(key,value){
                      tableBody = tableBody+'<tr>';
                      if(count==0){
                        tableHeader = tableHeader+'<tr>';
                      }
                      
                      if($.isPlainObject(value)){
                        $.each(value, function(k, v){
                          if(count==0){
                            tableHeader = tableHeader+'<th>'+k+'</th>';
                          }
                          
                          //console.log('Key: '+k+"<br> Value "+v);
                          tableBody = tableBody+'<td>'+v+'</td>';
                        });

                        count++;
                        tableHeader = tableHeader+'</tr>';
                      }
                      
                      tableBody = tableBody+'</tr>';
                  });
                  tableHeader = tableHeader+'</thead>';
                  tableBody = tableBody+"</tbody>";
                  //tableRow = tableRow+"</table>";
                  htmldata = tableMain+tableHeader+tableBody+"</table>";
                  
                    $('#'+modalId).html(htmldata);
                    var inst = $('[data-remodal-id='+modalId+']').remodal();
                    inst.open();
                    $('footable').footable();
                  
                   });
            
            
            
        }

        </script>
          <script>
        
       
		$(document).ready(function(){
			setTimeout(function(){
				$('#customlink').click();
			},10);
		});
		
		$('#customlink').on('click', function(){
			var inst = $('[data-remodal-id="myalert"]').remodal();
			inst.open();
		}); 
		</script>




</body>

<?php
	flush();
    ob_flush(); 
?>

</html>
