<?php
include_once('session_header.php');
include_once('function.php');
?>
 <!DOCTYPE html>
 <html >


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>User Management | WinLead Portal</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/sidemenu.css">

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />
    <link rel="stylesheet" href="css/footable.core.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">
     <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/bootstrap.min.js"></script>



</head>

<body class="fixed-navbar fixed-sidebar">
 <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
         <div class="splash-title">
            <h1>Astro - WinLead Portal</h1>
            <p> Please wait while page is loading....</p>
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>
        



<div class="wrapper">
<div class="content animate-panel">
 
  
    <div class="row" >

        <div class="col-lg-3 col-md-3">
              
          <div class="nav-side-menu">
     <div id="sidelogo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <?php genSideMenu() ?>
     </div>
</div>

    </div><!-- .span3 -->

    <div class="col-lg-9 col-md-9">
    <form class="form-horizontal unit" id ="prodct_ops_form" action="" method="post">


    <!-- Form Name -->
    <h2><i class = " fa fa-user"></i> User Management -Enable Users  </h2>




<!-- Text input-->
      <div class="light-green">



      <?php
      	addelem("boe");

      ?>



      </div>






      <div  class="typeahead" >  
      <button id="save" type="submit" name="save" class="btn btn-success btn-large"  ><i class="fa fa-floppy-o" aria-hidden="true"></i>  Publish</button>
      </div>

</form>
</div>
</div>
</div>
<script>
$('.datepicker').datepicker({
   dateFormat:"yy-mm-dd"
});

</script>
<!-- Vendor scripts -->
       
        <script src="js/index.js"></script>
        <script src="js/metisMenu.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script src="js/jquery.peity.min.js"></script>
        <script src="js/index.js"></script>

        <!-- App scripts -->
        <script src="js/homer.js"></script>
        <script src="js/footable.all.min.js"></script>
        <script src = "js/footable.paginate.js"></script>
        <script src="js/remodal.min.js"></script>
</body>
</html>

<?php
function addelem($elem){
$nameRealmMapping = array(
    "admin" => "Admin User",
    "agent"=>"Agent",
    "super"=>"Manager"
);



echo" <div class=\"row\" id=\"{$elem}pannel\">
 <table class=\"table table-striped\" id=\"{$elem}table\">
        <tr>
            
            <th>User Name</th>
           
        </tr>
        <tr>";
      echo "
        <td><input id=\"{$elem}[0][username]\" name=\"{$elem}[0][username]\" class=\"tarea\"></input></td>

        </tr>
        </table>
   <a style=\"margin-top:5px;margin-bottom:5px;margin-right:10px;float:right;padding:5px 20px 5px 20px; display:none;\" href=\"#\" id=\"remove{$elem}\" class=\"btn-info\">Remove rows</a>
   <a style=\"margin-top:5px;margin-bottom:5px;margin-right:10px;float:right;padding:5px 20px 5px 20px;\" href=\"#\" id=\"addNew{$elem}\" class=\"btn-info\">Add more rows</a>





</div>";


/*echo "
<script>
$(document).ready(function(e) {
var {$elem}table = $('#{$elem}table');
$('#addNew{$elem}').click(function(){
var i = $('#{$elem}table tr').size() + 1;
$('<tr><td><input id =\"{$elem}['+i+'][area]\" name=\"{$elem}['+i+'][area]\"  class =\"tarea\" ></input></td><td><input id=\"type\" name=\"{$elem}['+i+'][type]\"  class=\"tarea\" ></input></td><td><input id=\"input\" name=\"{$elem}['+i+'][issue]\" class=\"tarea\"></input></td><td><input id=\"input\" name=\"{$elem}['+i+'][action]\" class=\"tarea\"></input></td><td><input id=\"input\" name=\"{$elem}['+i+'][remark]\" class=\"tarea\"></input></td></tr>').appendTo({$elem}table);
i++;
$('#remove{$elem}').show();

return false;
});

$('#remove{$elem}').last().click(function(){
var i = $('#{$elem}table tr').size()
if(i==2){
$('#remove{$elem}').hide();
return false;
}
else{
$('#{$elem}table tr').last().remove();
 if(i==3){
    $('#remove{$elem}').hide();
    }
}
}) ;
});
</script>"; */


echo "<script>
$(document).ready(function(e) {
var {$elem}table = $('#{$elem}table');
$('#addNew{$elem}').click(function(){
var i = $('#{$elem}table tr').size() + 1;
$('<tr><td><input name=\"{$elem}['+i+'][username]\" class=\"tarea\"></input></td></tr>').appendTo({$elem}table);
i++;
$('#remove{$elem}').show();

return false;
});

$('#remove{$elem}').last().click(function(){
var i = $('#{$elem}table tr').size()
if(i==2){
$('#remove{$elem}').hide();
return false;
}
else{
$('#{$elem}table tr').last().remove();
 if(i==3){
    $('#remove{$elem}').hide();
    }
}
}) ;
});";?>

$( "form" ).on( "submit", function( event ) {
  event.preventDefault();
  dataString = $(this).serialize();
  decodedSting = decodeURIComponent(dataString);
  console.log(decodeURIComponent(dataString));


  request= $.ajax({
      type: "POST",
      url: "enable_process.php",
      data: decodedSting,
      success: function(response) {
        $('#prodct_ops_form').html("<div id='message'></div>");
        $('#message').html("<h2 class= 'astro' >Your Data has been Saved !</h2>"+response);
      }
  });

   return false;



 });<?php
echo "
</script>";

} ?>
