<?php
ini_set("date.timezone", "Asia/Kuala_Lumpur");

include_once('session_header.php');
include_once('function.php');


if(Input::exists('get')){

  $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
	
	
  //$data = file_get_contents("data/data.json");
  
  $name = Input::get('XID');
  
  if(is_null($name) or $name == '')
	  $name = 'OPENS';
  
  error_log($name);
  $rest = "http://172.18.37.201:8080/WinRest/Alert/".$name;
  
  error_log($rest);
  $data = file_get_contents($rest);
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
  $recoclass;
  
    $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	

	$time_elapsed_secs = microtime(true) - $start;
	error_log ('LEAD - Host: '.$_SERVER['REMOTE_ADDR'].' '.Input::get('XID').' '.$total_time,0);
}
else
{
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
	
	
  //$data = file_get_contents("data/data.json");
  
 
	  $name = 'OPENS';
  
  error_log($name);
  $rest = "http://172.18.37.201:8080/WinRest/Alert/".$name;
  
  error_log($rest);
  $data = file_get_contents($rest);
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
  $recoclass;
  
    $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	

	$time_elapsed_secs = microtime(true) - $start;
	error_log ('LEAD - Host: '.$_SERVER['REMOTE_ADDR'].' '.Input::get('XID').' '.$total_time,0);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	
    <!-- Page title -->
    <title>Campaign Management | WinLead Portal</title>

    <!-- Bootstrap core CSS -->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" rel="stylesheet">

	<!-- Prism -->
	<link href="css/prism.css" rel="stylesheet">

	<!-- FooTable Bootstrap CSS -->
	<link href="compiled/footable.bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/docs.css" rel="stylesheet">

<!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />

    <link rel="stylesheet" href="css/style.css">

	<link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />
    
</head>

<body class="docs">
	
<div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>

            </div>
             
           
            <?php generateNavBarNew($userRole,$role,$role2,$displayName,"Search Alert"); ?>

 </div>

        <!-- Main Wrapper -->
        <div class="content animate-panel">
        
            <div class="docs-section">
            
          
               <?php 
                if(!empty($jsonObj->WinLeadCamp->{'Alert Information'})){
				?>
				
				
                <div class="example">
                
				<?php 
              
                $panel_class = "highlight";
				
			    $showcols = array("ID" ,
								"MESSAGE",
								"PRIORITY",
								"ISACTIVE",
								"RAG",
                                  "CHECK"
                                  );
				 $toggle = array(
                                  "REMARKS"
                                  );

                $pagesize = 'data-page-size="50"';
                generateCampBlockNew("Existing Alerts",$pagesize,$jsonObj->WinLeadCamp->{'Alert Information'},$toggle,$showcols); 
				?>
				
				<div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
				
				<style scoped>
					/* provides a red astrix to denote required fields */
					.form-group.required .control-label:after {
						content:"*";
						color:red;
						margin-left: 4px;
					}
				</style>
				
				<div class="modal-dialog" role="document">
					<form class="modal-content form-horizontal" id="editor">
						<div class="modal-footer">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4  id="editor-title">Add Alert</h4>
						</div>
						<div class="modal-body">
							<input type="number" id="ID" name="ID" class="hidden"/>
							
							<div class="form-group required">
								<label for="MESSAGE" class="col-sm-3 control-label">Alert Message</label>
								<div class="col-sm-9">
									<textarea type="text" class="form-control" id="MESSAGE" name="MESSAGE" placeholder="Alert Message" ></textarea>
								</div>
							</div>
							
						<div class="form-group">
								<label for="PRIORITY" class="col-sm-3 control-label">PRIORITY</label>
								<div class="col-sm-9">
								<select class="form-control"  id="PRIORITY" name="PRIORITY">	
									<option type="text" class="form-control" value="1"  >1</option>
									<option type="text" class="form-control" value="2"  >2</option>
									<option type="text" class="form-control" value="3" >3</option>					
								</select>
								</div>
						</div>
						<div class="form-group">
								<label for="ISACTIVE" class="col-sm-3 control-label">IS ACTIVE?</label>
								<div class="col-sm-9">
								<select class="form-control"  id="ISACTIVE" name="ISACTIVE">	
									<option type="text" class="form-control" value="1"  >1</option>
									<option type="text" class="form-control" value="0"  >0</option>
								</select>
								</div>
								
						</div>
						<div class="form-group">
								<label for="RAG" class="col-sm-3 control-label">RAG STATUS</label>
								<div class="col-sm-9">
								<select class="form-control"  id="RAG" name="RAG">	
									<option type="text" class="form-control" value="R"  >R</option>
									<option type="text" class="form-control" value="A"  >A</option>
									<option type="text" class="form-control" value="G"  >G</option>
								</select>
								</div>
								
						</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Save changes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>

				</div>
				
              </div>

	
              <?php }
              ?>

    </div>
      


<!-- Placed at the end of the document so the pages load faster -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/prism.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Add in any FooTable dependencies we may need -->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<!-- Add in FooTable itself -->
<script src="compiled/footable.js"></script>
<!-- Initialize FooTable -->

<script>
	jQuery(function($){
		var $modal = $('#editor-modal'),
			$editor = $('#editor'),
			$editorTitle = $('#editor-title'),
			ft = FooTable.init('#editing-example', {
				editing: {
					enabled: true,
					addRow: function(){
						$modal.removeData('row');
						$editor[0].reset();
						$editorTitle.text('Add a new Alert....');
						$modal.modal('show');
					},
					editRow: function(row){
					//	confirm('in the edit');
						var values = row.val();
						$editor.find('#ID').val(values.ID);
						$editor.find('#MESSAGE').val(values.MESSAGE);
						$editor.find('#PRIORITY').val(values.PRIORITY);
						$editor.find('#ISACTIVE').val(values.ISACTIVE);
						$editor.find('#RAG').val(values.RAG);						
						console.log(values);
						$modal.data('row', row);
						$editorTitle.text('Edit row #' + values.ID);
						$modal.modal('show');
					},
					deleteRow: function(row){
						if (confirm('You Are Not Allowed to Delete Row')){
							//row.delete();
						}
					}
				}
			}),
			uid = parseInt($editor.find('#ID').val(),10);

		$editor.on('submit', function(e){
			if (this.checkValidity && !this.checkValidity()) return;
			e.preventDefault();
			
			var row = $modal.data('row'),
				values = ({
					ID : $editor.find('#ID').val(),
					MESSAGE : $editor.find('#MESSAGE').val(),
					ISACTIVE : $editor.find('#ISACTIVE  :selected' ).text(),
					PRIORITY : $editor.find('#PRIORITY  :selected' ).text(),
					RAG : $editor.find('#RAG  :selected' ).text(),
					PAYMENT : $editor.find('#PAYMENT  :selected' ).text()
				});

			if (row instanceof FooTable.Row){
				console.log(JSON.stringify(values));
				UpdateData(JSON.stringify(values));	
				row.val(values);
			} else {
				//values.ID = uid + 1;
				ft.rows.add(values);
				
				console.log(JSON.stringify(values));
				PostData(JSON.stringify(values));
			}
			
			$modal.modal('hide');
		});
	});

	
function PostData(values){
            //var table;
            var url;
              url = "http://172.18.37.201:8080/WinRest/Alert";
           

			var xhttp = new XMLHttpRequest();
			xhttp.open("POST", url, true);
			xhttp.setRequestHeader("Content-type", "application/json");
			xhttp.send(values);
			var response = xhttp.responseText;   
        }
function UpdateData(values){
            //var table;
            var url;
              url = "http://172.18.37.201:8080/WinRest/Alert";
          
			console.log(values);
			var xhttp = new XMLHttpRequest();
			xhttp.open("PUT", url, true);
			xhttp.setRequestHeader("Content-type", "application/json");
			xhttp.send(values);
			var response = xhttp.responseText;   
        }
</script>
</body>

<?php
	flush();
    ob_flush(); 
?>

</html>
