<?php
error_reporting(E_ERROR);


function createHeaderJSON($obj)
{
		$i = 0;
		if (!empty($obj)){
				foreach($obj as $prop => $value)
				{
						if ($i==0) {
						foreach($value as $prop => $value)
						{
												if (!is_array($value)) {
														echo '<th style="font-size: 10px;outline: thin solid orange;">' . $prop . '</th>';
												}

										$i++;
							}
						}



			 }
		}


}

function createRowsJSON($obj)
{
		if (!empty($obj)){
				foreach($obj as $prop => $value)
				{
						echo '<tr>';
						foreach($value as $prop => $value)
						{
									if (!is_array($value)) {
														echo '<td style="font-size: 10px;outline: thin solid orange;">' . $value . '</td>';
									}


						}
						echo '</tr>';

			 }
		}


}

function fetchUsers($UserID=null){

	$user = 'appuser';
	$pass = 'appuser';
	$aLink='<a href="/editUser.php?XID=ReplaceMe"><u>EditMe</u></a>';
	
	$sql = "SELECT  replace('".$aLink."','ReplaceMe',username) Edit,username,name,role,winback,lead,change_pass,disabled FROM users";
	if (is_null($UserID)==false){
		$sql=$sql." where username='".$UserID."'";
	}

	$retVal=array();
	try {
		$dbh = new PDO('mysql:host=172.18.108.52;dbname=winlead', $user, $pass);
    foreach($dbh->query($sql) as $row) {
        $retVal[]=$row;
    }
		$dbh = null;
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
	}
 
  return json_encode(array('UserDetails'=>$retVal));
}

function check_digit($AccountID){
  
  $tot;
  $num;
  $i;
 
  $num = $AccountID;
  $tot = 0;
  $i = 9;

  while ($i>1) {
    $tot = $tot + ($num % 10) * (11 - $i);
      $num = floor(($num/10.00)); 
      $i=$i-1;
  }
  
  $i = $tot%11;

  if ($i >= 10) {
    $i=0;
  }
    

  return $i;
}


function generateAccountInfo($jsonObj){
  $color_class;
  $btncls;
  $textclass = "default";
  if($jsonObj->AccStatus=='Active'){
    $color_class= "green-bg";
    $btncls="btn-success";
  }
  else{
    $color_class= "red-bg";
    $btncls="btn-danger";
  }
  echo '<div class="hpanel middle_panel">

        <div class="panel-heading '.$color_class.'">
              <div class="">
                    <div class="left">
                        Account Information
                    </div>
                    <div class="right">

                              <span class="sbutton '.$btncls.'">
                                  '.$jsonObj->Bill_Type.'
                              </span>

                    </div>
              </div>

        </div>
        <div class="panel-body">

                                        <div class="table-responsive">
                                            <table class="footable table table-striped">
                                                <tbody style="text-overflow: ellipsis;">
                                                    <tr>
                                                        <td><span class="">BillCycle#</span></td>
                                                        <td>'.$jsonObj->BILL_CYCLE.'</td>
                                                    </tr>

                                                    <tr>
                                                        <td><span class="">BillFreq#</span></td>
                                                        <td>'.$jsonObj->Bill_Freq.'</td>
                                                    </tr>

                                                    <tr>
                                                        <td><span class="">PTPViolated#</span></td>
                                                        <td>'.$jsonObj->ptp_violated.'</td>
                                                    </tr>

                                                    <tr>
                                                        <td><span class="">PayMethod#</span></td>
                                                        <td>'.$jsonObj->pay_method.'</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="">CCInfo#</span></td>
                                                        <td>'.$jsonObj->CC_info.'</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

          </div>
             
      </div>';


}

function generateCollectionBucket($jsonObj){
  $color_class;
  $btncls;
  $textclass = "default";
  if($jsonObj->WinBack->AccountInformation[0]->CREDIT_CLASS=='LO'){
    $color_class= "green-bg";
    $btncls="btn-success";
  }else if($jsonObj->WinBack->AccountInformation[0]->CREDIT_CLASS=='ME')
  {
    $color_class= "amber-bg";
    $btncls="btn-warning";

  }
  else{
    $color_class= "red-bg";
    $btncls="btn-danger";
  }


echo '
<div class="hpanel middle_panel">

  <div class="panel-heading '.$color_class.'">
        <div class="">
              <div class="left">
                  Collection Bucket
              </div>
              <div class="right">

                        <span class="sbutton '.$btncls.'">
                            '.$jsonObj->WinBack->AccountInformation[0]->CREDIT_CLASS.'
                        </span>

              </div>
        </div>

  </div>

    <div class="panel-body">

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Bucket</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span class="font-bold">1 - 30</span>
                            </td>
                            <td>
                                '.$jsonObj->WinBack->CollectionBucket[0]->bucket_1.'
                            </td>
                        </tr>
                        <tr>
                            <td>';

                if ($jsonObj->WinBack->AccountInformation[0]->CREDIT_CLASS=='HI') {
                  if ((int)$jsonObj->WinBack->CollectionBucket[0]->bucket_2>0){
                    $textclass='text-danger';
                  }

                }

                if ($jsonObj->WinBack->AccountInformation[0]->CREDIT_CLASS=='ME') {
                  if ((int)$jsonObj->WinBack->CollectionBucket[0]->bucket_2>0){
                    $textclass='text-warning';
                  }else{
                        $textclass='text-primary';
                  }

                }


              echo '<span class="'.$textclass.'">31 - 60</span>
                            </td>
                            <td>'.
                              $jsonObj->WinBack->CollectionBucket[0]->bucket_2.'
                            </td>
                        </tr>
                        <tr>
                        <td>';

              if ((int)$jsonObj->WinBack->CollectionBucket[0]->bucket_3>0){
                $textclass='text-danger';
              }

              if ($jsonObj->WinBack->AccountInformation[0]->CREDIT_CLASS=='LO') {
                if ((int)$jsonObj->WinBack->CollectionBucket[0]->bucket_3>0){
                  $textclass='text-warning';
                }else{
                      $textclass='text-primary';
                }

              }else{
                    $textclass='text-primary';
              }
          echo '<span class="'.$textclass.'">61 - 90</span>
                            </td>
                            <td>
                              '.$jsonObj->WinBack->CollectionBucket[0]->bucket_3.'
                            </td>
                        </tr>
                        <tr>
                            <td>';
            if ((int)$jsonObj->WinBack->CollectionBucket[0]->bucket_4>0){
                              $textclass='text-danger';
                }else{
                      $textclass='text-primary';
                }

              echo
                                    '<span class="'.$textclass.'">>90</span>
                            </td>
                            <td>
                                '.$jsonObj->WinBack->CollectionBucket[0]->bucket_4.'
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>



    </div>

    </div>';
}




//Function to generateTableHeader();

function generateTableHeader($jsonObj,$toggle,$showcols,$type){

echo '<thead>
          <tr>';
		 
          foreach((array)$jsonObj as $header=>$value){
            if(is_array($value)||is_object($value)){
              // Just ignore array
            }else{
                if(count($showcols)==0){
                   echo '<th class="cellborder">' . $header . '</th>';

                }else{
                    if(in_array($header,$toggle)){
                      echo '<th  class="cellborder" data-toggle="true">' . $header . '</th>';
                    }else if(in_array($header,$showcols))
                    {
                      echo '<th  class="cellborder">' . $header . '</th>';

                    }else{
                      echo '<th  class="cellborder" data-hide="all">' . $header . '</th>';

                    }

                }
            }


            }
             if($type=='eacts'){
                      echo '<th  class="cellborder">' ."Interaction" . '</th>';
                      echo '<th  class="cellborder">' ."SMC Detail" . '</th>';
              }
           
echo'     </tr>
     </thead>';


}

//Function to Geneate the TableBody

function generateTableBody($jsonObj,$type){
  if(!empty($jsonObj)){
  $count = 0;
  $acct_id;
  
  echo ' <tbody>';
  


  foreach ($jsonObj as $subObj) {
    
    if(!is_object($subObj)&&!is_array($subObj)){
       echo '<td  class="cellborder">'.$subObj.'</td>';
    }
    else{
      if(is_object($subObj)){
              echo '<tr  class="cellborder">';
            foreach ($subObj as $key => $value) {
				
				
				
              if(is_object($value)||is_array($value)){
              }else{
                if($key=='Account ID'){
                  $acct_id = $value;
                }
                echo '<td  class="cellborder" data-id="'.$value.'">'.$value.'</td>';
                
              }
              
            }
            if($type=='eacts'){
              echo '<td  class="cellborder"><button class="btn btn-small btn-info interaction" data-acts="'.$acct_id.'">Interaction</button></td>';
              echo '<td  class="cellborder"><button class="btn btn-small btn-info smc" data-acts="'.$acct_id.'">SMC Details</button></td>';
            }
            echo '</tr>';
          

        }else{
          /*  echo '<tr>';
            foreach ($subObj as $value) {
              if(is_object($value)||is_array($value)){
              }else{
                echo '<td data-id="'.$value.'">'.$value.'</td>';
              }
              
            }
            echo '</tr>';
            */
        }
      }
    
  
  }

  echo ' </tr></tbody> ';
  
  
}
}

function generateTableFooter($colspan){
 echo '<tfoot class="hide-if-no-paging">
          <tr>
            <td  class="cellborder" colspan="'.$colspan.'" class="text-center">
              <ul class="pagination">
              </ul>
            </td>
          </tr>
       </tfoot>';
    
}


//Function to generateTableHeader();

function generateCampTableHeaderNew($jsonObj,$toggle,$showcols,$type){

echo '<thead>
          <tr>';
		 
          foreach((array)$jsonObj as $header=>$value){
            if(is_array($value)||is_object($value)){
              // Just ignore array
            }else{
                if(count($showcols)==0){
                   echo '<th data-name = "'. $header .'" class="cellborder">' . $header . '</th>';

                }else{
                    if(in_array($header,$toggle)){
                      echo '<th data-name = "'. $header .'" class="cellborder" data-breakpoints="all">' . $header . '</th>';
                    }
					else if(in_array($header,$showcols))
                    {
                      echo '<th  data-name = "'. $header .'" class="cellborder" data-breakpoints="xs">' . $header . '</th>';

                    }
					else{
						if($header=="ID")
						echo '<th  data-name = "'. $header .'" class="cellborder" data-breakpoints="xs" data-type="number" data-toggle="true">' . $header . '</th>';
						else
						echo '<th  data-name = "'. $header .'" class="cellborder" data-hide="all" data-toggle="true">' . $header . '</th>';							
                    }

                }
            }


            }
             
           
echo'     </tr>
     </thead>';


}

//Function to Geneate the TableBody
function generateCampTableBodyNew($jsonObj,$type){
  if(!empty($jsonObj)){
  $count = 0;
  $acct_id;
  
  echo ' <tbody>';
  
  foreach ($jsonObj as $subObj) {
    
    if(!is_object($subObj)&&!is_array($subObj)){
       echo '<td  class="cellborder">'.$subObj.'</td>';
    }
    else{
      if(is_object($subObj)){
              echo '<tr  data-expanded="false" class="cellborder">';
            foreach ($subObj as $key => $value) {
			if(is_object($value)||is_array($value)){
              }
			  else{
                if($key=='CAMPAIGN'){
                  $camp_id = $value;
                }
				if($key=='PROMO CODE'){
                  $promo_id = $value;
                }
                echo '<td  class="cellborder" >'.$value.'</td>';
                
              }
              
            }
            
            echo '</tr>';
          

        }
      }
    
  
  }

  echo ' </tbody> ';
  
  
}
}
function generateCampBlockNew($panelHeader,$pagesize,$obj,$toggle,$showcols,$panel_class="middle1"){
  $type = "";
  $pagesize="";
  if(is_array($obj)){
    $headerobj = $obj[0];
  }else{
    $headerobj = $obj;
  }
  
  echo'
                        

                          <table id="editing-example" class="table" data-toggle-column="first" data-paging="true" data-filtering="true" data-sorting="true">';
                                generateCampTableHeaderNew($headerobj,$toggle,$showcols,$type);
                                generateCampTableBodyNew($obj,$type);
                                //generateCampTableFooterNew(count($showcols)+1);

                     echo' </table>
                        


                 ';

}

//Function to generateTableHeader();

function generateAlertTableHeaderNew($jsonObj,$toggle,$showcols,$type){

echo '<thead>
          <tr>';
		 
          foreach((array)$jsonObj as $header=>$value){
            if(is_array($value)||is_object($value)){
              // Just ignore array
            }else{
                if(count($showcols)==0){
                   echo '<th data-name = "'. $header .'" class="cellborder">' . $header . '</th>';

                }else{
                    if(in_array($header,$toggle)){
                      echo '<th data-name = "'. $header .'" class="cellborder" data-breakpoints="all">' . $header . '</th>';
                    }
					else if(in_array($header,$showcols))
                    {
                      echo '<th  data-name = "'. $header .'" class="cellborder" data-breakpoints="xs">' . $header . '</th>';

                    }
					else{
						if($header=="ID")
						echo '<th  data-name = "'. $header .'" class="cellborder" data-breakpoints="xs" data-type="number" data-toggle="true">' . $header . '</th>';
						else
						echo '<th  data-name = "'. $header .'" class="cellborder" data-hide="all" data-toggle="true">' . $header . '</th>';							
                    }

                }
            }


            }
             
           
echo'     </tr>
     </thead>';


}

//Function to Geneate the TableBody
function generateAlertTableBodyNew($jsonObj,$type){
  if(!empty($jsonObj)){
  $count = 0;
  $acct_id;
  
  echo ' <tbody>';
  
  foreach ($jsonObj as $subObj) {
    
    if(!is_object($subObj)&&!is_array($subObj)){
       echo '<td  class="cellborder">'.$subObj.'</td>';
    }
    else{
      if(is_object($subObj)){
              echo '<tr  data-expanded="false" class="cellborder">';
            foreach ($subObj as $key => $value) {
			if(is_object($value)||is_array($value)){
              }
			  else{
                if($key=='CAMPAIGN'){
                  $camp_id = $value;
                }
				if($key=='PROMO CODE'){
                  $promo_id = $value;
                }
                echo '<td  class="cellborder" >'.$value.'</td>';
                
              }
              
            }
            
            echo '</tr>';
          

        }
      }
    
  
  }

  echo ' </tbody> ';
  
  
}
}
function generateAlertBlockNew($panelHeader,$pagesize,$obj,$toggle,$showcols,$panel_class="middle1"){
  $type = "";
  $pagesize="";
  if(is_array($obj)){
    $headerobj = $obj[0];
  }else{
    $headerobj = $obj;
  }
  
  echo'
                        

                          <table id="editing-example" class="table"  data-paging="true"  >';
                                generateCampTableHeaderNew($headerobj,$toggle,$showcols,$type);
                                generateCampTableBodyNew($obj,$type);
                                //generateCampTableFooterNew(count($showcols)+1);

                     echo' </table>
                        


                 ';

}




function generatePanelBlock($panelHeader,$pagesize,$obj,$toggle,$showcols,$panel_class="middle"){
  $type = "";
  $pagesize="";
  if(is_array($obj)){
    $headerobj = $obj[0];
  }else{
    $headerobj = $obj;
  }
  if($panelHeader=='Existing Account'){
    $details = '<a id="interaction" href="#pendingOrders" class="btn btn-info btn-small">Pending Orders</a>';
    $icon = '<i class="fa fa-list-ul" aria-hidden="true"></i>';
    $type = "eacts";
  }else{
    $details = "&nbsp";
    $icon = '<i class="fa fa-file-o" aria-hidden="true"></i>';
  }
  echo'<div class="hpanel '.$panel_class.'">
                        <div class="panel-heading">

                            <div class="">
                                  <div class="left">'.$icon.' '
                                      .$panelHeader.'
                                  </div>
                                  <div class="right">
                                      '.$details.'
                                  </div>
                            </div>

                        </div>

                        <div class="panel-body '.$panel_class.'" id=""> 
                          <table class="footable scroll table table-bordered table-hover" '.$pagesize.' toggle ="false"  data-sort="false">';
                                generateTableHeader($headerobj,$toggle,$showcols,$type);
                                generateTableBody($obj,$type);
                                //generateTableFooter(count($showcols)+1);

                     echo' </table>
                        </div>


                  </div>';

}

function genSideMenuCamp(){
  echo '
 <ul id="menu-content" class="menu-content collapse out">
                        <li>
                          <a href="campMgmt.php">
                          <i class="fa fa-dashboard fa-lg"></i> Add Campaign
                          </a>
                        </li>
                        <li>
                          <a href="disable.php">
                          <i class="fa fa-user fa-lg"></i> Edit  Campaign
                          </a>
                          </li>
                        <li>
                          <a href="index.php">
                          <i class="fa fa-home fa-lg"></i> Home
                          </a>
                        </li>


                    </ul>
  ';


}

function genSideMenu(){
  echo '
 <ul id="menu-content" class="menu-content collapse out">
                        <li>
                          <a href="userMgmt.php">
                          <i class="fa fa-dashboard fa-lg"></i> Add Users
                          </a>
                        </li>
                        <li>
                          <a href="disable.php">
                          <i class="fa fa-user fa-lg"></i> Disable Users
                          </a>
                          </li>
                           <li>
                              <a href="enable.php">
                              <i class="fa fa-user fa-lg"></i> Enable Users
                              </a>
                          </li>
                  


                         <li>
                          <a href="resetUser.php">
                          <i class="fa fa-users fa-lg"></i> Reset Password
                          </a>
                        </li>
                        <li>
                          <a href="index.php">
                          <i class="fa fa-home fa-lg"></i> Home
                          </a>
                        </li>


                    </ul>
  ';


}

?>
