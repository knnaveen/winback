<?php
class User{
    private $_db,
            $_ldap = null,
            $_user,
            $_group,
            $_isLoggedin = false,
            $_sessionName,
            $_sessionRole,
            $_sessionRole2,
            $_displayName,
            $dbhash,
            $_cookieName,
            $_userRole;

  // Constructor for LDAP and DB
  public function __construct(){

          $this->_db = DB::getInstance();
          $this->_sessionName = Config::get('session/session_name');
          $this->_sessionRole = Config::get('session/session_role');
          $this->_sessionRole2 = Config::get('session/session_role2');
          $this->_userRole =    Config::get('session/userRole');
          $this->_displayName = Config::get('session/displayName');
          $this->_cookieName  = Config::get('logged/cookie_name');

          if(Session::exists($this->_sessionName)){
            $this->_isLoggedin = true;
          }
  }


//
  public function login($username = null,$password=null,$role){
    //var_dump($this->_ldap->getLdapConnection());
    $hash = '';
    $logged = false;
    $admin = '';
    if($this->_db){
    if($username&&$password){
      $roletype = $role=='winback'?'winback':'lead';
      $role2=null;
      $this->_db->get('users',array(
        array('username','=',$username),
        array('password','=',md5($password)),
        array($roletype,'=','1'),
        array('disabled','=','0')
      ));
      if($this->_db->count()){
          $logged = true; //apply local auth
          if($this->_db->first()->change_pass==1){
              Session::put($this->_sessionName,$username);
              Redirect::to('changePass.php');
          }
      }else{
        return false;
      }


        if($logged){
              Session::put($this->_sessionName,$username);
              Session::put($this->_displayName,$this->_db->first()->name);
              Session::put($this->_userRole,$this->_db->first()->role);
              $admin = $this->_db->first()->role;
              Session::put($this->_sessionRole,$roletype);
              Session::put($this->_userRole,$admin);
              if($roletype=='winback' && $this->_db->first()->lead==1){
                $role2="lead";
              }else if($roletype=='lead' && $this->_db->first()->winback==1){
                $role2="winback";
              }
              Session::put($this->_sessionRole2,$role2);
              $this->_isLoggedin = true;

              $hashCheck = $this->_db->get('table_user_sessions',
                  array(
                      array('username','=',$username),
                      array('agent','=',$_SERVER['HTTP_USER_AGENT'])
                      ));
              if(!$hashCheck->count()){
                $salt = Hash::salt(16);
                $hash = Hash::unique();
                $hasedcookie = Hash::make($hash,$salt);
                $this->_db->insert('table_user_sessions',
                    array(
                      'username' => $username,
                      'hash' => $hasedcookie,
                      'salt' => $salt,
                      'agent'=> $_SERVER['HTTP_USER_AGENT']
                      ));
                $store = $hash."|".$username."|".Session::get($this->_displayName)."|".$roletype."|".$role2."|".$admin;
                //echo $store;
                Cookie::put($this->_cookieName,$store,Config::get('logged/cookie_expiry'));

                }else{
                  //var_dump($hashCheck->results());
                  $dbhash =  $hashCheck->first()->hash;

                  //$hasedcookie = $hashCheck->first()->hash;
                }
              //$store = $hash.",".$username;
              //Cookie::put($this->_cookieName,$store,Config::get('logged/cookie_expiry'));

            return true;
        }
    }}
    else{
      echo 'DB Not connected';
    }
    return false;
  }

  public function isLoggedIn(){
    return $this->_isLoggedin;
  }


}
