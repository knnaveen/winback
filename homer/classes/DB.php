<?php

/*
* Mysql database class - only one connection alowed
*/
class DB {
	private $_connection;
	private static $_instance = null; //The single instance
	private $_pdo,
			$_sql,
			$_error = false,
			$_result,
			$_count = 0;

	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new DB();
		}
		return self::$_instance;
	}
	// Constructor
	private function __construct() {
		try{
		$this->_pdo = new PDO('mysql:host='.Config::get('dbConfig/server').';port=3306;dbname='.Config::get('dbConfig/dbname'),
		Config::get('dbConfig/user'),Config::get('dbConfig/password'));
		//$this->_connection = new mysqli(Config::get('dbConfig/server'), Config::get('dbConfig/user'), Config::get('dbConfig/password'),Config::get('dbConfig/dbname'));
		$this->_connection = $this->_pdo;
		}catch (mysqli_sql_exception $e){
			die($e->getMessage());
		}
		// Error handling
		/*
		if(mysqli_connect_error()) {
			trigger_error("Failed to conencto to MySQL: " . mysqli_connect_error(),
				 E_USER_ERROR);
		}*/
		
	}
	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }
	// Get mysqli connection
	public function getConnection() {
		return $this->_connection;
	}

	// function to query will work on the PDO Object only
	public function query($sql, $params = array()){
		$this->_error = false;
		if($this->_sql = $this->_pdo->prepare($sql)) {
			$index = 1;
					if(count($params)){
							foreach($params as $param => $val){
								$this->_sql->bindValue($index,$val);
								$index++;
							}
					}
					if($this->_sql->execute()){
						$this->_result = $this->_sql->fetchAll(PDO::FETCH_OBJ);
						$this->_count = $this->_sql->rowCount();

					}else{
						$this->_error = true;
					}
			}
			return $this;
   }
	 public function error(){
		 return $this->_error;
	 }
	 public function count(){
		 return $this->_count;
	 }
	 // Perform SQL commonds //select /delete

	 private function action($action,$table,$where = array()){
			$opertors = array('=','>','<','>=','<=');
			$evalCond ='';
			$cnt = count($where);
			$x = 1;
			$value = array();
			foreach($where as $cond){
				if(count($cond)==3){
				$column_name = $cond[0];
				$operator = $cond[1];
				array_push($value,$cond[2]);
						if(in_array($operator,$opertors)){
						if($cnt==1 || $x==1){
							$evalCond.= " {$column_name} {$operator} ?";
						}else if($x==($cnt))
						{
							$evalCond.= " AND {$column_name} {$operator} ?";

						}else{
							$evalCond.= " AND {$column_name} {$operator} ?";
						}

					}
					$x++;
				}
		}
			$sql = "{$action} FROM {$table} WHERE {$evalCond} ";
		  if(!$this->query($sql,$value)->error()){
					return $this;
			}

			return false;
	 }
// Get the data from table
	public function get($table,$where)
	{
		return $this->action('SELECT *', $table,$where);
		# code...
	}

	public function delete($table,$where){
				return $this->action('DELETE', $table,$where);
		}
	 //Perfrom sql
	// Retunn the result SELECT
	public function results(){
		return $this->_result;
	}


// inserting the data
public function insert($table,$fields = array()){
		if(count($fields)){
			$keys = array_keys($fields);
			$values = null;
			$cnt = 1;
			foreach ($fields as $field) {
				# code...
				$values.="?";
				if($cnt < count($fields)){
						$values .=',';
				}
				$cnt++;
			}
			$sql = "INSERT INTO {$table} (`".implode('`,`',$keys)."`) VALUES ({$values})" ;
			
			if(!$this->query($sql,$fields)->error()){
				return true;
			}
		}
		return false;
}

// Function to update the database
public function update($table,$id=array(),$fields){
	$set = '';
	$cnt = 1;
	$where  ='';
	foreach($fields as $clname => $value){
		$set .= "{$clname}=?";
		if($cnt<count($fields)){
			$set .= ',';
		}
		$cnt++;
	}
	$cnt = 1;
	foreach($id as $clname => $value){
		$where .= " {$clname}='{$value}'";
		if($cnt<count($id)){
			$where .= ' AND ';
		}
		$cnt++;
	}
	$sql  = "UPDATE {$table} SET {$set} WHERE {$where}";

	if(!$this->query($sql,$fields)->error()){
		return true;
	}
	return false;
}


// Function to return the first rowCount
public function first(){
	return $this->results()[0];
}


}
?>
