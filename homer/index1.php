<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Astro | WinLead Portal</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />

    <link rel="stylesheet" href="css/style.css">


</head>

<body class="fixed-navbar fixed-sidebar">

    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
        <div class="splash-title">
            <h1>Astro - WinLead Portal</h1>
            <p> Please wait while page is loading....</p>
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>
    <!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <!-- Header -->

    <?

  function topNavItems($cls,$Val){

   echo '<div style="width: 12%; float:left; margin: 2px;">
      <p><button class="btn '.$cls.'" style="font-size: 12px;width: 100%;">'.$Val.'</button></p>
    </div>';

  }

  function leftNavItems($Hdr,$Val){

    echo '<tr>
              <td style="width: auto;">'.$Hdr.'</td>
              <td style="max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">'.$Val.'</td>
          </tr>';

  }

  function createTabeJSON($obj,$tabID,$clsID,$th)
  {
      $i = 0;
      if (!empty($obj)){
          foreach($obj as $prop => $value)
          {
              $value1 = $value;
              echo '<table id="'.$tabID.'" class="'.$clsID.'">';
              echo '<thead class="'.$th.' table-fixed" style="background-color: blue;">';
              echo '<tr>';
              if ($i==0) {
              foreach($value as $prop => $value)
              {

                          if (!is_array($value)) {
                              echo '<th>' . $prop . '</th>';
                          }

                      $i++;
                }
              }

              echo '</tr></thead>';

              echo '<tbody class="'.$th.'">';
              echo '<tr>';

              foreach($value1 as $prop => $value)
              {
                          if (!is_array($value)) {
                            //	echo '<td>' . $value . '</td>';
                              echo '<td><div style="height:40px; overflow:hidden">'.$value.'</div></td>';

                          }
              }

              echo '</tr></tbody>';

              echo '</table';

            }
      }


  }


  function createHeaderJSON($obj)
  {
      $i = 0;
      if (!empty($obj)){
          foreach($obj as $prop => $value)
          {
              if ($i==0) {
              foreach($value as $prop => $value)
              {
                          if (!is_array($value)) {
                              echo '<th style="font-size: 10px;outline: thin solid orange;">' . $prop . '</th>';
                          }

                      $i++;
                }
              }



         }
      }


  }

  function createRowsJSON($obj)
  {
      if (!empty($obj)){
          foreach($obj as $prop => $value)
          {
              echo '<tr>';
              foreach($value as $prop => $value)
              {
                    if (!is_array($value)) {
                              echo '<td style="font-size: 10px;outline: thin solid orange;">' . $value . '</td>';
                    }


              }
              echo '</tr>';

         }
      }


  }

  function fetchIntrxn($output){

    $LeadObj=$output['WinBack']['IntrxnInformation'];

    if (!empty($LeadObj)){
        foreach($LeadObj as $prop => $value)
        {
            $value1 = $value;

            //echo $value['Media'];

            echo '<div class="">';



              switch ($value['Media']) {
                case 'Call':
                          echo '<span><img  src="img/call.png" /></span>';
                          break;
                case 'SMS':
                          echo '<span><img  src="img/sms.png" /></span>';
                          break;
                case 'HEADEND':
                          echo '<span"><img  src="img/vod.png" /></span>';
                          break;
                case 'Others':
                          echo '<span><img  src="img/media.png" /></span>';
                          break;
                default:
                        echo '<span><img  src="img/media.png" /></span>';
              }

            echo '</div';



         }
     }
  }

  $rest = file_get_contents("data/wb.json");
  $output = json_decode($rest, true);


  //$rest = "http://172.18.37.201:8080/WinBackRest/fetchwbAPIinfo/" . $_GET['XID'];
  //$data = file_get_contents($rest);

  //$output = json_decode($data, true);

  if(empty($output)){

    echo '<div class="alert alert-danger">
               <strong>Account ID : '.$_GET['XID'].'</strong> not found!.
          </div>';

    die();

  }

  //var_dump($output);

  $CustObj=$output['WinBack']['CustomerInformation'];
  $AccObj=$output['WinBack']['AccountInformation'];
  $CollObj=$output['WinBack']['CollectionBucket'];
  $AddObj=$output['WinBack']['AddressInformation'];
  $PayObj=$output['WinBack']['PaymentInformation'];
  $SmcObj=$output['WinBack']['SMCInformation'];
  $IntrxnObj=$output['WinBack']['IntrxnInformation'];
  $FinObj=$output['WinBack']['FinancialInformation'];
?>

        <div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
            <img src="img/astro_image.png">
        </span>
            </div>

            <nav role="navigation">

                <form role="search" class="navbar-form" method="get" action="index.php">
                    <div class="form-group">
                        <input type="text" name="XID" placeholder="Search Account here!" class="form-control" name="search">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </div>
                </form>

                <div class="mobile-menu">
                    <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <div class="collapse mobile-navbar" id="mobile-collapse">
                        <ul class="nav navbar-nav">
                            <li>
                                <a class="" href="login.html">Login</a>
                            </li>
                            <li>
                                <a class="" href="login.html">Logout</a>
                            </li>
                            <li>
                                <a class="" href="profile.html">Profile</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="navbar-right">
                    <ul class="nav navbar-nav no-borders">
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-th-list"></i>
                            </a>

                            <div class="dropdown-menu hdropdown">

                                <div class="hpanel" style="max-width: 500px; max-height: 500px;">
                                    <div class="panel-body">
                                        <div class="panel panel-default" style="max-width: 500px; max-height: 500px;">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Interaction Information</h3>
                                            </div>

                                            <div class="table-responsive" style="max-width: 300px; max-height: 300px;">
                                                <table class="table table-hover" style="max-width: 100%;text-overflow: ellipsis;">
                                                    <thead>
                                                        <tr>
                                                            <? createHeaderJSON($IntrxnObj);  ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <? createRowsJSON($IntrxnObj); ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <i class="pe-7s-keypad"></i>
                            </a>

                            <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="projects.html">
                                                    <i class="pe pe-7s-portfolio text-info"></i>
                                                    <h5>Projects</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="mailbox.html">
                                                    <i class="pe pe-7s-mail text-warning"></i>
                                                    <h5>Email</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="contacts.html">
                                                    <i class="pe pe-7s-users text-success"></i>
                                                    <h5>Contacts</h5>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="forum.html">
                                                    <i class="pe pe-7s-comment text-info"></i>
                                                    <h5>Forum</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="analytics.html">
                                                    <i class="pe pe-7s-graph1 text-danger"></i>
                                                    <h5>Analytics</h5>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="file_manager.html">
                                                    <i class="pe pe-7s-box1 text-success"></i>
                                                    <h5>Files</h5>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown">
                                <i class="pe-7s-mail"></i>
                                <span class="label label-success">4</span>
                            </a>
                            <ul class="dropdown-menu hdropdown animated flipInX">
                                <div class="title">
                                    You have 4 new messages
                                </div>
                                <li>
                                    <a>
                                It is a long established.
                            </a>
                                </li>
                                <li>
                                    <a>
                                There are many variations.
                            </a>
                                </li>
                                <li>
                                    <a>
                                Lorem Ipsum is simply dummy.
                            </a>
                                </li>
                                <li>
                                    <a>
                                Contrary to popular belief.
                            </a>
                                </li>
                                <li class="summary"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" id="sidebar" class="right-sidebar-toggle">
                                <i class="pe-7s-upload pe-7s-news-paper"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="login.html">
                                <i class="pe-7s-upload pe-rotate-90"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>



        <!-- Main Wrapper -->
        <div id="wrapper">

            <div class="content animate-panel">

              <div class="row">

                <div class="col-lg-3">
                    <div class="hpanel">
                        <div class="panel-body text-center h-100">

                            <h3 class="m-xs">

                            <b><? echo $CustObj[0]['Full_name'];?></b>

                            <div class="progress m-t-xs full progress-small">
                                <div style="width: 100%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100"

                                    <?
                                      $btncls='progress-bar-info';

                                      $btncls="progress-bar-danger";
                                      if ($AccObj[0]['AccStatus']=='Active'){
                                       $btncls="progress-bar-success";
                                       }

                                    ?>

                                     role="progressbar" class=" progress-bar <? echo $btncls; ?>">

                                </div>
                            </div>

                          </h3>

                            <h4 class="no-margins">
                                <aside><b><u>(<?echo $CustObj[0]['ID_Type'].' - '.$CustObj[0]['ID'] ?>)</u></b></aside>

                                </h4>
                                <small><aside>Mobile# <u><?echo $CustObj[0]['MOBILE_PHONE'] ?></u></aside>
                          <aside>Home Ph.# <?echo $CustObj[0]['Home_Phone'] ?></aside>
                          <aside>Office Ph.# <?echo $CustObj[0]['Office_Phone'] ?></aside>
                          <aside><?echo $CustObj[0]['e_mail'] ?> </aside></small>
                        </div>

                    </div>
                </div>

                <div class="col-lg-9">
                      <div class="hpanel stats">
                          <div class="panel-body h-200">

                            <div class="row">
                              <div class="stats-title pull-left">

                                  <h3>Acc Information</h3>
                              </div>

                              <div class="stats-icon pull-right">
                                  <?
                               $btncls="btn-danger";
                               if ($AccObj[0]['AccStatus']=='Active'){
                                $btncls="btn-success";
                                }
                              ?>
                                      <button class="<? echo $btncls; ?>">
                                          <? echo $AccObj[0]['AccStatus']; ?>
                                      </button>
                              </div>
                            </div>
                            <div class="row">

                                      <div class="table-responsive">
                                          <table class="table table-striped">
                                              <tbody style="text-overflow: ellipsis;">
                                                  <tr>
                                                      <td><span class="">BillCycle#</span></td>
                                                      <td><?echo $AccObj[0]['BILL_CYCLE']; ?></td>
                                                  </tr>

                                                  <tr>
                                                      <td><span class="">BillFreq#</span></td>
                                                      <td><?echo $AccObj[0]['Bill_Freq']; ?></td>
                                                  </tr>

                                                  <tr>
                                                      <td><span class="">PTPVoilated#</span></td>
                                                      <td><?echo $AccObj[0]['ptp_voilated']; ?></td>
                                                  </tr>

                                                  <tr>
                                                      <td><span class="">PayMethod#</span></td>
                                                      <td><?echo $AccObj[0]['pay_method']; ?></td>
                                                  </tr>

                                                  <tr>
                                                      <td><span class="">CCInfo#</span></td>
                                                      <td>CC Type: AMERICAN EXPRESS Exp. MM YYYY: 12 2016 Last Four: 6911</td>
                                                  </tr>
                                              </tbody>
                                          </table>
                                      </div>

                                      <!--
                                      <div class="row">
                                          <div class="col-xs-6">
                                              CC Info
                                          </div>

                                          <div class="col-xs-6">
                                              <? echo $CustObj[0]['CC_info']; ?>
                                          </div>
                                      </div>
                                     -->


                            </div>



                          </div>

                      </div>
                </div>


                <div class="col-lg-9 pull-left border-right">

                  <div class="row">

                    <div class="col-lg-2 pull-left border-right tdiv">
                         <aside style="text-align:center;">AR Balance</aside>
                         <aside style="text-align:center;"><h1><? echo $CustObj[0]['Balance']; ?></h1></aside>

                    </div>

                    <div class="col-lg-2 pull-left border-right tdiv">
                         <aside style="text-align:center;">Balance Due</aside>
                         <?
                           $txtclass="";
                           if ((int)$CustObj[0]['PasDueAmount']>0) {
                              $txtclass="text-warning";
                           }
                           if ((int)$CollObj[0]['PasDueAmount']>=10) {
                              $txtclass="text-danger";
                           }
                         ?>
                         <aside style="text-align:center;" class="<?echo $txtclass;?>"><h1><? echo $CollObj[0]['PasDueAmount']; ?></h1></aside>

                    </div>

                    <div class="col-lg-2 pull-left border-right tdiv">
                         <aside style="text-align:center;">Debt Age</aside>
                         <aside style="text-align:center;" class="<?echo $txtclass;?>">
                           <h1><? echo $AccObj[0]['DebtAge'];?></h1></aside>
                    </div>

                    <div class="col-lg-2 pull-left border-right tdiv">
                         <aside style="text-align:center;">Invoice Age</aside>
                         <aside style="text-align:center;" class="<?echo $txtclass;?>">
                           <h1><? echo $AccObj[0]['Invoice_Age'];?></h1></aside>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-lg-4 pull-left">
                      <div class="well well-sm divboxblue">
                          <h4 class="blue">Service Address</h4>
                          <?
                          echo $AddObj[0]['ServiceAddress'];
                      ?>
                      </div>
                    </div>
                    <div class="col-lg-4 pull-left">
                        <div class="well well-sm divboxblue">
                            <h4 class="blue">Billing Address</h4>
                            <?
                            echo $AddObj[0]['BillingAddress'];
                        ?>
                        </div>
                    </div>

                    <div class="col-lg-4 pull-left">
                        <div class="well well-sm divboxblue">
                            <h4 class="blue">Mailing Address</h4>
                            <?
                            echo $AddObj[0]['MailingAddress'];
                        ?>
                        </div>
                      </div>
                    </div>

                </div>

              </div>






                <div class="row">



                    <div class="col-lg-3">
                        <div class="hpanel">
                            <div class="panel-body text-center h-200">

                                <h3 class="m-xs">

                                <b><? echo $CustObj[0]['Full_name'];?></b>

                                <div class="progress m-t-xs full progress-small">
                                    <div style="width: 100%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100"

                                        <?
                                          $btncls='progress-bar-info';

                                          $btncls="progress-bar-danger";
                                          if ($AccObj[0]['AccStatus']=='Active'){
                                           $btncls="progress-bar-success";
                                           }

                                        ?>

                                         role="progressbar" class=" progress-bar <? echo $btncls; ?>">

                                    </div>
                                </div>

                              </h3>

                                <h4 class="no-margins">
                                    <aside><b><u>(<?echo $CustObj[0]['ID_Type'].' - '.$CustObj[0]['ID'] ?>)</u></b></aside>

                                    </h4>
                                    <small><aside>Mobile# <u><?echo $CustObj[0]['MOBILE_PHONE'] ?></u></aside>
                              <aside>Home Ph.# <?echo $CustObj[0]['Home_Phone'] ?></aside>
                              <aside>Office Ph.# <?echo $CustObj[0]['Office_Phone'] ?></aside>
                              <aside><?echo $CustObj[0]['e_mail'] ?> </aside></small>

                                  <br>

                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <tbody>
                                                <tr>
                                                    <td><span class="">SubType#</span></td>
                                                    <td><?echo $CustObj[0]['SubType']; ?></td>
                                                </tr>

                                                <tr>
                                                    <td><span class="">Type#</span></td>
                                                    <td><?echo $CustObj[0]['Type']; ?></td>
                                                </tr>

                                                <tr>
                                                    <td><span class="">Race#</span></td>
                                                    <td><?echo $CustObj[0]['Race']; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>


                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="hpanel stats">
                            <div class="panel-body h-200">
                                <div class="stats-title pull-left">

                                    <h3>Collection Bucket</h3>
                                </div>

                                <div class="stats-icon pull-right">
                                    <?
                                  $btncls="btn-danger";
                                  if ($AccObj[0]['CREDIT_CLASS']=='LO'){
                                    $btncls="btn-success";
                                  }

                                  if ($AccObj[0]['CREDIT_CLASS']=='ME'){
                                    $btncls="btn-warning";
                                  }
                                ?>
                                        <button class="<? echo $btncls; ?>">
                                            <? echo $AccObj[0]['CREDIT_CLASS']; ?>
                                        </button>
                                </div>

                                <div class="m-t-xl">
                                    <h4 class="m-b-xs">Amt Due: <? echo $CollObj[0]['PasDueAmount']; ?></h4>
                                    <span class="font-bold no-margins">
                              AR Balance: <? echo $CustObj[0]['Balance']; ?>
                          </span>

                                    <div class="progress m-t-xs full progress-small">
                                        <div style="width: 100%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" <? $btncls='progress-bar-info' ; if ((int)$CollObj[0][ 'PasDueAmount']>=20 && $AccObj[0]['CREDIT_CLASS']=='LO' && (int)$AccObj[0]['DebtAge']>=40){ $btncls='progress-bar-danger'; } if ((int)$CollObj[0]['PasDueAmount']>=20 && $AccObj[0]['CREDIT_CLASS']=='ME' && (int)$AccObj[0]['DebtAge']>=15){ $btncls='progress-bar-danger'; } if ((int)$CollObj[0]['PasDueAmount']>=10 && $AccObj[0]['CREDIT_CLASS']=='HI' && (int)$AccObj[0]['DebtAge']>=1){ $btncls='progress-bar-danger'; } ?> role="progressbar" class=" progress-bar
                                            <? echo $btncls; ?>">

                                        </div>
                                    </div>




                                </div>
                            </div>

                        </div>
                    </div>



                    <div class="col-lg-3">
                        <div class="hpanel stats">
                            <div class="panel-body h-200">
                                <div class="stats-title pull-left">

                                    <h3>Acc Information</h3>
                                </div>

                                <div class="stats-icon pull-right">
                                    <?
                                 $btncls="btn-danger";
                                 if ($AccObj[0]['AccStatus']=='Active'){
                                  $btncls="btn-success";
                                  }
                                ?>
                                        <button class="<? echo $btncls; ?>">
                                            <? echo $AccObj[0]['AccStatus']; ?>
                                        </button>
                                </div>

                                <div class="m-t-xl">



                                    <?
                                    $btncls='';

                                    if ((int)$CollObj[0]['PasDueAmount']>=20
                                          && $AccObj[0]['CREDIT_CLASS']=='LO'
                                          && (int)$AccObj[0]['DebtAge']>=40){
                                      $btncls='text-danger';
                                    }

                                    if ((int)$CollObj[0]['PasDueAmount']>=20
                                          && $AccObj[0]['CREDIT_CLASS']=='ME'
                                          && (int)$AccObj[0]['DebtAge']>=15){
                                      $btncls='text-danger';
                                    }

                                    if ((int)$CollObj[0]['PasDueAmount']>=10
                                          && $AccObj[0]['CREDIT_CLASS']=='HI'
                                          && (int)$AccObj[0]['DebtAge']>=1){
                                      $btncls='text-danger';
                                    }

                                  ?>
                                        <h4 class="<? echo $btncls; ?>">Debt Age: <? echo $AccObj[0]['DebtAge'];?></h4>


                                        <span class="font-bold no-margins">
                                      Invoice Age: <? echo $AccObj[0]['Invoice_Age'];?>
                                  </span>

                                        <div class="progress m-t-xs full progress-small">
                                            <div style="width: 100%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" <? $btncls='progress-bar-info' ; if ((int)$CollObj[0][ 'PasDueAmount']>=20 && $AccObj[0]['CREDIT_CLASS']=='LO' && (int)$AccObj[0]['DebtAge']>=40){ $btncls='progress-bar-danger'; } if ((int)$CollObj[0]['PasDueAmount']>=20 && $AccObj[0]['CREDIT_CLASS']=='ME' && (int)$AccObj[0]['DebtAge']>=15){ $btncls='progress-bar-danger'; } if ((int)$CollObj[0]['PasDueAmount']>=10 && $AccObj[0]['CREDIT_CLASS']=='HI' && (int)$AccObj[0]['DebtAge']>=1){ $btncls='progress-bar-danger'; } ?> role="progressbar" class=" progress-bar
                                                <? echo $btncls; ?>">

                                            </div>
                                        </div>


                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <tbody style="text-overflow: ellipsis;">
                                                    <tr>
                                                        <td><span class="">BillCycle#</span></td>
                                                        <td><?echo $AccObj[0]['BILL_CYCLE']; ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td><span class="">BillFreq#</span></td>
                                                        <td><?echo $AccObj[0]['Bill_Freq']; ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td><span class="">PTPVoilated#</span></td>
                                                        <td><?echo $AccObj[0]['ptp_voilated']; ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td><span class="">PayMethod#</span></td>
                                                        <td><?echo $AccObj[0]['pay_method']; ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td><span class="">CCInfo#</span></td>
                                                        <td>CC Type: AMERICAN EXPRESS Exp. MM YYYY: 12 2016 Last Four: 6911</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!--
                                        <div class="row">
                                            <div class="col-xs-6">
                                                CC Info
                                            </div>

                                            <div class="col-xs-6">
                                                <? echo $CustObj[0]['CC_info']; ?>
                                            </div>
                                        </div>
                                       -->




                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="hpanel stats">
                            <div class="panel-body h-200">

                                <div class="well well-sm divboxblue">
                                    <h4 class="blue">Service Address</h4>
                                    <?
                                    echo $AddObj[0]['ServiceAddress'];
                                ?>
                                </div>

                                <div class="well well-sm divboxblue">
                                    <h4 class="blue">Billing Address</h4>
                                    <?
                                    echo $AddObj[0]['BillingAddress'];
                                ?>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>




                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <div class="panel-body" id="subsinfo">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Subscription Status Information</h3>
                                            </div>

                                            <div class="table-responsive" style="height: 80%; width: auto;">
                                                <table class="table table-hover" style="max-width: 100%;text-overflow: ellipsis;">
                                                    <thead>
                                                        <tr>
                                                            <? createHeaderJSON($SmcObj);  ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <? createRowsJSON($SmcObj); ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <div class="panel-body" id="payinfo">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="panel panel-default" style="height: 200px;">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Payment History</h3>
                                            </div>
                                            <div class="table-responsive" style="height: 80%; width: 100%">
                                                <table class="table table-hover" style="max-width: 100%;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                                                    <thead>
                                                        <tr>
                                                            <? createHeaderJSON($PayObj);  ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <? createRowsJSON($PayObj); ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="panel panel-default" style="height: 200px;">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Financial History</h3>
                                            </div>
                                            <div class="table-responsive" style="height: 80%; width: 100%;">
                                                <table class="table table-hover" style="max-width: 100%;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                                                    <thead>
                                                        <tr>
                                                            <? createHeaderJSON($FinObj);  ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <? createRowsJSON($FinObj); ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Right sidebar -->

            <!-- Footer-->
            <footer class="footer">
                <span class="pull-right">
            Powered by Astro Design Authority
        </span> Astro Copy Right @ 2015-2016
            </footer>

        </div>

        <!-- Vendor scripts -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.flot.js"></script>
        <script src="js/jquery.flot.resize.js"></script>
        <script src="js/jquery.flot.pie.js"></script>
        <script src="js/curvedLines.js"></script>
        <script src="js/index.js"></script>
        <script src="js/metisMenu.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script src="js/jquery.peity.min.js"></script>
        <script src="js/index.js"></script>

        <!-- App scripts -->
        <script src="js/homer.js"></script>
        <script src="js/charts.js"></script>

        <script>
            $(function() {

                /**
                 * Flot charts data and options
                 */
                var data1 = [
                    [0, 55],
                    [1, 48],
                    [2, 40],
                    [3, 36],
                    [4, 40],
                    [5, 60],
                    [6, 50],
                    [7, 51]
                ];
                var data2 = [
                    [0, 56],
                    [1, 49],
                    [2, 41],
                    [3, 38],
                    [4, 46],
                    [5, 67],
                    [6, 57],
                    [7, 59]
                ];

                var chartUsersOptions = {
                    series: {
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1,
                            fill: 0.4
                        },
                    },
                    grid: {
                        tickColor: "#f0f0f0",
                        borderWidth: 1,
                        borderColor: 'f0f0f0',
                        color: '#6a6c6f'
                    },
                    colors: ["#62cb31", "#efefef"],
                };

                $.plot($("#flot-line-chart"), [data1, data2], chartUsersOptions);

                /**
                 * Flot charts 2 data and options
                 */
                var chartIncomeData = [{
                    label: "line",
                    data: [
                        [1, 10],
                        [2, 26],
                        [3, 16],
                        [4, 36],
                        [5, 32],
                        [6, 51]
                    ]
                }];

                var chartIncomeOptions = {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 0,
                            fill: true,
                            fillColor: "#64cc34"

                        }
                    },
                    colors: ["#62cb31"],
                    grid: {
                        show: false
                    },
                    legend: {
                        show: false
                    }
                };

                $.plot($("#flot-income-chart"), chartIncomeData, chartIncomeOptions);



            });

        </script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-4625583-2', 'webapplayers.com');
            ga('send', 'pageview');

        </script>

</body>

</html>
