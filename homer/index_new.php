<?php
include_once('session_header.php');
include_once('function.php');

if(Input::exists('get')){
	ob_flush();
    flush();
    $rest = "http://172.18.108.52:8080/wbapi/wb/FETCHWBAPIINFO/" .Input::get('XID');
    $data = file_get_contents($rest);
    $jsonObj = json_decode($data,false);
    $output = json_decode($data, true);

}



//$rest = "http://172.18.37.201:8080/WinBackRest/fetchwbAPIinfo/" . $_GET['XID'];
//  $data = file_get_contents($rest);

//$output = json_decode($data, true);



?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-cache" />

    <!-- Page title -->
    <title>Astro | WinLead Portal</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link rel="stylesheet" href="css/metisMenu.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="css/helper.css" />
    <link rel="stylesheet" href="css/footable.core.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/remodal.css">
    <link rel="stylesheet" href="css/remodal-default-theme.css">





</head>

<body class="fixed-navbar fixed-sidebar">

    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
        <div class="splash-title">
            <h1>Astro - WinLead Portal</h1>
            <p> Please wait while page is loading....</p>
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>
    <!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

    <!-- Header -->

    <?php




  

  

 
  //$rest = "http://172.18.37.201:8080/WinBackRest/fetchwbAPIinfo/" . $_GET['XID'];
//  $data = file_get_contents($rest);

  //$output = json_decode($data, true);


  //var_dump($output);
if(!empty($jsonObj->WinBack->CustomerInformation)){
  $CustObj=$jsonObj->WinBack->CustomerInformation[0];
  $AddObj=$output['WinBack']['AddressInformation'];
}
?>

        <div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>
            </div>

          
            <div class="name">
            <?php if(!empty($CustObj)){
                 echo $CustObj->Full_name ;
               }
            ?>
            
            </div>
            <?php 
            if(!empty($jsonObj->WinBack->AccountInformation[0])){
            $class = $jsonObj->WinBack->AccountInformation[0]->AccStatus=='InActive'?'redbg':'greenbg'; 
            echo '<span class="status '.$class.'">'
                              .$jsonObj->WinBack->AccountInformation[0]->AccStatus.'
              </span>';
       
            if ($jsonObj->WinBack->CustomerInformation[0]->VIP=='Y') {
          echo '<span class="status" style="margin-top: -5px;" ><img src="img/VIP.png"/> </span>';
        }
      }      
      
      
            ?>
            
            <?php generateNavBar($userRole,$role,$role2,$displayName); ?>
           
        </div>

      

        <!-- Main Wrapper -->
        <div id="wrapper">
          <?php 
        if(!empty($jsonObj->WinBack)){

        ?>

            <div class="content animate-panel">
              <div class="row">
                  <div class="col-xs-12 col-lg-3 col-sm-4 col-md-4">

                    <?php $class = $jsonObj->WinBack->AccountInformation[0]->AccStatus=='InActive'?'red-bg':'green-bg'; ?>
                    <div class="hpanel topleft">
                      <div class="panel-heading <?php echo $class ;?>">
                            <div >
                                  <div class="left">
                                      Account Details
                                  </div>
                                  <div class="right">
                                    <i class="fa fa-user phone"><strong> 0<?php echo $jsonObj->WinBack->AccountInformation[0]->ACCOUNT_ID ?><?php echo check_digit($jsonObj->WinBack->AccountInformation[0]->ACCOUNT_ID); ?>  </strong></i>
                                  </div>
                            </div>

                      </div>

                  <div class="panel-body ">
                    <div class="table-responsive">
                                    <table class="footable table table-striped">
                                        <tbody>
                                            <tr>
                                              <td><?php  echo $CustObj->ID_Type ;?></td>
                                              <td><?php echo $CustObj->ID ?></td>
                                            </tr>


                                            <tr>
                                              <td>Mobile#</td>
                                              <td><?php echo $CustObj->MOBILE_PHONE ?></td>
                                            </tr>

                                              <tr>
                                                <td>Home#</td>
                                                <td><?php  echo $CustObj->Home_Phone ?></td>
                                              </tr>

                                                <tr>
                                                  <td>Office#</td>
                                                  <td><?php echo $CustObj->Office_Phone ?></td>
                                                </tr>

                                                  <tr>
                                                    <td>e-Mail#</td>
                                                    <td><?php echo $CustObj->e_mail ?></td>
                                                  </tr>

                                            <tr>
                                                <td><span class="">SubType#</span></td>
                                                <td><?php echo $CustObj->SubType ; ?></td>
                                            </tr>

                                            <tr>
                                                <td><span class="">Type#</span></td>
                                                <td><?php echo $CustObj->Type ?></td>
                                            </tr>

                                            <tr>
                                                <td><span class="">Race#</span></td>
                                                <td><?php echo $CustObj->Race ; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                        </div>

                    </div>
              </div>

              <div class="col-xs-12 col-lg-9 col-sm-8 col-md-8 gutterSpace ">
                <div class="row whitebg gutterSpace1 " >

                  <?php
                  $curr =  $jsonObj->WinBack->CollectionBucket[0]->Curr_charges;
          
          if ($curr<=0){
            $curr=0;
          }
                  $class = 'infobg';
                  //$class= $jsonObj->WinBack->AccountInformation[0]->CREDIT_CLASS=='HI'?'redbg':'greenbg';
                  echo '<div class="col-md-2 col-xs-6 ">
                    <div class="top_title '.$class.'">Current Charges</div>
                    <div class="top_content '.$class.'">'.$curr.'</div>
                  </div>';


                  $class= $jsonObj->WinBack->CollectionBucket[0]->PasDueAmount>0?'redbg':'greenbg';
                  echo '<div class="col-md-2 col-xs-6 ">
                    <div class="top_title '.$class.'">Amount Due</div>
                    <div class="top_content '.$class.'">'.$jsonObj->WinBack->CollectionBucket[0]->PasDueAmount.'</div>
                  </div>';

                  $class= $jsonObj->WinBack->CollectionBucket[0]->ar_balance>10?'redbg':'greenbg';
                  echo '<div class="col-md-2 col-xs-6 ">
                    <div class="top_title '.$class.'">AR Balance</div>
                    <div class="top_content '.$class.'">'.$jsonObj->WinBack->CollectionBucket[0]->ar_balance.'</div>
                  </div>';



                    $class = 'greenbg';
                    $class= $jsonObj->WinBack->AccountInformation[0]->DebtAge>30?'redbg':'greenbg';
                    echo '<div class="col-md-2 col-xs-6 ">
                      <div class="top_title '.$class.'">Debt Age</div>
                      <div class="top_content '.$class.'">'.$jsonObj->WinBack->AccountInformation[0]->DebtAge.'</div>
                    </div>';

                    $class = 'greenbg';
                    $class= $jsonObj->WinBack->AccountInformation[0]->Invoice_Age>60?'redbg':'greenbg';
                    echo '<div class="col-md-2 col-xs-6 ">
                      <div class="top_title '.$class.'">Invoice Age</div>
                      <div class="top_content '.$class.'">'.$jsonObj->WinBack->AccountInformation[0]->Invoice_Age.'</div>
                    </div>';




                    echo '<div class="col-md-2 col-xs-6">
                      <div class="top_title infobg">Bill Cycle : '.$jsonObj->WinBack->AccountInformation[0]->BILL_CYCLE.'</div>
                      <div class="top_content infobg">Frequency :'.$jsonObj->WinBack->AccountInformation[0]->Bill_Freq.'</div>
                    </div>'

                  ?>





              </div>



                <div class="row margin-20 gutterSpace">

                  <!-- collection bucket -->
                    <div class="col-lg-4">
                      <?php generateCollectionBucket($jsonObj); ?>
                    </div>



                    <div class="col-lg-4 gutterSpace">
                    <?php  generateAccountInfo($jsonObj->WinBack->AccountInformation[0]); ?>
                    </div>
                    <div class="col-lg-4">


                        <div class="hpanel middle_panel gutterSpace">

                          <div class="panel-heading grey-bg">
                                <div class="">
                                      <div class="left">
                                          Address Information
                                      </div>
                                      <div class="right">
                                          <i class="home">&nbsp</i>
                                      </div>
                                </div>
                            </div>


                            <div class="panel-body">

                                <div class="address">
                                    <div class="address_title">Service Address</div>
                                    <div class="address_detail">
                                    <?php
                                      echo $AddObj[0]['ServiceAddress'];
                                    ?>
                                    </div>
                                </div>

                                <div class="address">
                                    <div class="address_title">Billing Address</div>
                                     <div class="address_detail">
                                      <?php
                                      echo $AddObj[0]['BillingAddress'];
                                      ?>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

              </div>
        </div>


                <div class="row mypannel" >

                    <!--Subcription History-->

                      <div class="col-lg-4 col-md-4 col-xs-12" >
                       <?php 
					   //style="height: 56.25vh;"
                          if(empty($jsonObj->WinBack->SMCInformation)){
                          //Do nothing
                            }else{
                                $toggle = array('SMC','STATUS');
                                $showcols = array('STATUS_DATE');

                              $count = count($jsonObj->WinBack->SMCInformation);
                              $pagesize = "";
                              if($count>3){
                                $pagesize = 'data-page-size="4"';
                              }
                            
              $msg="Subscriber Status Information";
                $cnt=count($jsonObj->WinBack->SMCInformation);
              
              if ($cnt>0){
				if ($jsonObj->WinBack->SMCInformation[0]->SMC<>null){
					$msg=$msg." (SMC : ".$cnt.")";
				}else{
					$msg=$msg." (SMC : 0)";
				}
                
              }

                            generatePanelBlock($msg,$pagesize,$jsonObj->WinBack->SMCInformation,$toggle,$showcols,'mypannel');
                          }

                          ?>
                          
                      </div>
                        <!--Subcription ends here-->
                  
                    <!--Payment Start here -->
                        <div class="col-md-4 col-lg-4 col-xs-12 gutterSpace">
                          <?php 
                          if(empty($jsonObj->WinBack->PaymentInformation)){
                          //Do nothing
                            }else{
                               $toggle = array('Method');
                               $showcols = array('DepositDt','Amt','SRC_ID','PostDt');

                              $count = count($jsonObj->WinBack->PaymentInformation);
                              $pagesize = "";
                              if($count>3){
                                $pagesize = 'data-page-size="4"';
                              }
                          

                            generatePanelBlock("Payment Information",$pagesize,$jsonObj->WinBack->PaymentInformation,$toggle,$showcols,'mypannel');
                          }

                          ?>
                     </div>
                  <!--Payment ends here-->

                    <!--Finacialinformation Start here -->
                    <div class="col-md-4 col-lg-4 col-xs-12 gutterSpace">
                      <?php 
                      if(empty($jsonObj->WinBack->FinancialInformation)){
                      //Do nothing
                        }else{
                          $toggle = array('PostDt');
                          $showcols = array('Description','Amount','TrnsType','UserID');

                          $count = count($jsonObj->WinBack->FinancialInformation);
                          $pagesize = "";
                          if($count>3){
                            $pagesize = 'data-page-size="4"';
                          }
                      

                        generatePanelBlock("Financial Information",$pagesize,$jsonObj->WinBack->FinancialInformation,$toggle,$showcols,'mypannel');
                      }

                      ?>

                    </div>


                    <!--Financial Information end here-->



                </div>

                <div class="row">

                    <!-- Fist Panel goes here -->

                    <div class="col-lg-10 col-xs-12">
                      <div class="col-lg-3 col-xs-12" >

                              
								<div class="right">
								   <button id="intrxn" data-intrxn="<?php echo Input::get('XID'); ?>"  
								         class="btn btn-warning btn-large col-xs-12"> Interactions Details</button>
							    </div>
             
                  </div>


                  <!--- Second One -->

                  <div class="col-xs-12 col-lg-3">


                      <div class="hpanel bottom ">

                        <div class="right">
								   <button id="disconnect" data-disconnect="<?php echo Input::get('XID'); ?>"  
								         class="btn btn-warning btn-large col-xs-12"> Disconnection Details</button>
					    </div>


                          
                     </div>
                </div>


                <!---Third Pannel-->

                <div class="col-lg-3 col-xs-12">


                    <div class="right">
                         <a id="billing" href="#" data-bill="<?php echo Input::get('XID'); ?> " class="btn btn-warning btn-large col-xs-12"> Billing Details</a>
                    </div>
                </div>

                <!---4th Pannel -->

                <div class="col-lg-3 col-xs-12">


                    <div class="right">
								   <button id="fp" data-fp="<?php echo Input::get('XID'); ?>"  
								         class="btn btn-warning btn-large col-xs-12"> Free Preview Details</button>
					</div>
          
              </div>
              </div>
              <div class="col-lg-2">
                <div class="row">
                  <div class="col-xs-12">

                      <div class="right">
                           <button id="flash" data-flash="<?php echo Input::get('XID'); ?>"  class="btn btn-warning btn-large col-xs-12"> Flash Message Details</button>
                      </div>
                  
                  </div>
                </div>
              </div>

                </div>
                <div class="remodal" data-remodal-id="modal">
                  <button data-remodal-action="close" class="remodal-close"></button>
                  <h1>Interactions</h1>
                  <p>
                   <?php 
                      if(empty($jsonObj->WinBack->IntrxnInformation)){
                      //Do nothing
                        }else{
                          $toggle = array('START_TIME');
                          $showcols = array('Title_Type','Media','Direction');

                          $count = count($jsonObj->WinBack->IntrxnInformation);
                          $pagesize = "";
						  
                          if($count>10){
                            $pagesize = 'data-page-size="200"';
                          }
                      

                       // generatePanelBlock("",$pagesize,$jsonObj->WinBack->IntrxnInformation,$toggle,$showcols,"full-height");
                      }

					  foreach ($output['WinBack']['IntrxnInformation'] as $key => $value) {
						  
						  
						echo '<div class="v-timeline vertical-container animate-panel" data-child="vertical-timeline-block" data-delay="1">
						<div class="vertical-timeline-block">';  
					    $img_path;
						
						switch ($value['Media']) {
							case "Call":
								$img_path="img/call.png";
								break;
							case "IVR":
								$img_path="img/media.png";
								break;
							case "SMS":
								$img_path="img/sms.png";
								break;
							case "VOD":
								$img_path="img/vod.png";
								break;
							default:
								$img_path="img/media.png";
						}
						
						echo '<div class="vertical-timeline-icon navy-bg">
								<img src="'.$img_path.'"/>
							</div>';
							
						echo '<div class="vertical-timeline-content">
								<div class="p-sm">
									<span class="vertical-date pull-right"> Start Time: <small>'.$value['START_TIME'].'</small> </span>

									<h2 class="vertical-date pull-left" >Title: '.$value['Title'].'</h2>
									
									Media: '.$value['Media'].', Direction: '.$value['Direction'].'
									
								</div>
								<div class="panel-footer">
								<p align="left">Text: '.$value['Text'].'</p>
								</div>
							</div>';
						
						
						echo '</div>	
							</div>';
						
						
						
					  
					  }
					  
                      ?>
					  
					  
					  
                  </p>
                  <br>
                  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>



                  <div class="remodal" data-remodal-id="disconnection">
                  <button data-remodal-action="close" class="remodal-close"></button>
                  <h1>Disconnection Information</h1>
                  <p>
                   <?php 
                      if(empty($jsonObj->WinBack->DisconnectInformation)){
                      //Do nothing
                        }else{
                          $toggle = array('SMC');
                          $showcols = array('OrderType','OrderType');

                          $count = count($jsonObj->WinBack->DisconnectInformation);
                          $pagesize = "";
                          if($count>10){
                            $pagesize = 'data-page-size="50"';
                          }
                      

                        generatePanelBlock("",$pagesize,$jsonObj->WinBack->DisconnectInformation,$toggle,$showcols,"full-height");
                      }

                      ?>
                  </p>
                  <br>
                  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>





                  <div class="remodal" data-remodal-id="free">
                  <button data-remodal-action="close" class="remodal-close"></button>
                  <h1>Free Preview Information</h1>
                  <p>
                   <?php 
                      if(empty($jsonObj->WinBack->FreepreviewInformation)){
                      //Do nothing
                        }else{
                          $toggle = array('SMC');
                          $showcols = array('Status','ChannelName','ChannelCode','StartDate','EndDate');

                          $count = count($jsonObj->WinBack->FreepreviewInformation);
                          $pagesize = "";
                          if($count>10){
                            $pagesize = 'data-page-size="50"';
                          }
                      

                        generatePanelBlock("",$pagesize,$jsonObj->WinBack->FreepreviewInformation,$toggle,$showcols,"full-height");
                      }

                      ?>
                  </p>
                  <br>
                  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                  </div>
          
          
            <!--Bill Details -->

                    <div class="remodal" data-remodal-id="bill">
					  <br>
                      <button data-remodal-action="cancel" class="remodal-close"></button>
                      

                      <div class="">
                          <div class="row">
                          <div class="col-md-12"><h3>Bill Details</h3>
                          <div class="color-line"></div>
                                    
                                  <!-- tabs left -->
                                  <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs" role="tablist">
                                      <li><h4><strong>Invoice Date</strong></h4> </li>
                                    </ul>
                                    <div class="tab-content pull-right" style="width:78%">
                                    
                                    </div>
                                  </div>
                              <!-- /tabs -->
                              
                            </div>
                        </div>
                        </div>
                                                
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                      
                  </div>
				  
				   <!--Bill Details -->

                    <div class="remodal" data-remodal-id="flashmsg">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      

                      <div class="">
                          <div class="row">
                          <div class="col-md-12"><h3>Flash Messages</h3>
                          <div class="color-line"></div>
                                    
                                  <!-- tabs left -->
                                  <div class="table-responsive" id="">
                                   <table class="table table-bordered table-condensed table-hover" id="myflash" ></table>
                                  </div>
                              <!-- /tabs -->
                              
                            </div>
                        </div>
                        </div>
                                                
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                      
                  </div>
				  
				  <div class="remodal" data-remodal-id="intrxnmsg">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      

                      <div class="">
                          <div class="row">
                          <div class="col-md-12"><h3>Interaction Details</h3>
                          <div class="color-line"></div>
                                    
                                  <!-- tabs left -->
                                  <div class="table-responsive" id="">
                                   <table class="table table-bordered table-condensed table-hover" id="myintrxn" ></table>
                                  </div>
                              <!-- /tabs -->
                              
                            </div>
                        </div>
                        </div>
                                                
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                      
                  </div>
				  
				  <div class="remodal" data-remodal-id="disconnectmsg">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      

                      <div class="">
                          <div class="row">
                          <div class="col-md-12"><h3>Disconnection Information</h3>
                          <div class="color-line"></div>
                                    
                                  <!-- tabs left -->
                                  <div class="table-responsive" id="">
                                   <table class="table table-bordered table-condensed table-hover" id="mydisconnect" ></table>
                                  </div>
                              <!-- /tabs -->
                              
                            </div>
                        </div>
                        </div>
                                                
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                      
                  </div>
				  
				  
				  <div class="remodal" data-remodal-id="fpmsg">
                      <button data-remodal-action="close" class="remodal-close"></button>
                      

                      <div class="">
                          <div class="row">
                          <div class="col-md-12"><h3>Free Preview Information</h3>
                          <div class="color-line"></div>
                                    
                                  <!-- tabs left -->
                                  <div class="table-responsive" id="">
                                   <table class="table table-bordered table-condensed table-hover" id="myfp" ></table>
                                  </div>
                              <!-- /tabs -->
                              
                            </div>
                        </div>
                        </div>
                                                
                      <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                      
                  </div>
				  






            <!-- Right sidebar -->

          <?php } ?>

        </div>

        <!-- Vendor scripts -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <script src="js/index.js"></script>
        <script src="js/metisMenu.min.js"></script>
        <script src="js/icheck.min.js"></script>
        <script src="js/jquery.peity.min.js"></script>
        <script src="js/index.js"></script>

        <!-- App scripts -->
        <script src="js/homer.js"></script>
        <script src="js/footable.all.min.js"></script>
        <script src = "js/footable.paginate.js"></script>
        <script src="js/remodal.min.js"></script>
        <script src="js/fixheader.js"></script>
        

        <script>
        $(function(){
            $('.footable').footable();
        });
        $(function(){
          //$('.footable').fixedHeaderTable('show');
        });
        </script>

    
    <script type="text/javascript">
      
$(function(){



    $('#billing').on('click', function(){
    var accountid = $(this).data('bill');
    var me = $(this);
    if(me.data('running')){
      return false;
    }
    me.data('running',true);
    var acurl = "http://172.18.108.52:8080/wbapi/wb/fetchbillinfo/"+accountid;
            $.getJSON(acurl).done(function(data){
                  //var dataKey = data.BillInformation;
				  //console.log(data);
                  var count = 0;
                  var cls = ""
                  var invoiceid;
                  var tabHtml = '<div role="tabpanel" class="tab-pane activeclass" id="ref"></div>';
          var highlightTab = '<div class="col-xs-12 col-md-4"><div class="hpanel"><div class="panel-heading">colTitle</div><div class="panel-heading">accnum</div></div></div>';
          var invSumTable = '<div class="table-responsive"><table id="invsum" class="table table-condensed"><th>Prev Month Balance</th><th>Payment</th><th>OverDue Charges</th><th>New Charges</th><th>Total Amount Due</th><tr><table>';
                  $('.tab-pane').empty();
          $('.nav-tabs').empty();
                  $.each(data.BillInformation, function(key,val){
					  //console.log(val);
             var invRow = '<tr><td>bal</td><td>pay</td><td>over</td><td>newcharge</td><td>total</td></tr>';
             var highRow = '<div class="row">';
             invRow = invRow.replace('bal', val.prev_balance);
             invRow = invRow.replace('pay', val.Payment);
             invRow = invRow.replace('over', val.OverDue_Charges);
             invRow = invRow.replace('newcharge', val.New_Charges);
             invRow = invRow.replace('total', val.Total_Amt_due);
             var hightabCopy = highlightTab;
               hightabCopy = highlightTab.replace('colTitle',"Account ID");
             hightabCopy = hightabCopy.replace('accnum',val.BA_ACCOUNT_NO);
             highRow = highRow+hightabCopy;
             hightabCopy = highlightTab.replace('colTitle',"Invoice Number");
			 //console.log(val.InvoiceNumber);
             hightabCopy = hightabCopy.replace('accnum',val.InvoiceNumber);
             highRow = highRow+hightabCopy;
             hightabCopy = highlightTab.replace('colTitle',"Pay By Date");
             hightabCopy = hightabCopy.replace('accnum',val.PayBy);
             highRow = highRow+hightabCopy;
                        //var listItem = '<li><a href="#a" data-toggle="value"> value</a></li>';
                        if(count===0){
                          cls = "active";
                          count++;
                          tabHtml = tabHtml.replace('activeclass',cls);
                        }else{
                           tabHtml = tabHtml.replace('ac',"");
                        }
                        $('.nav-tabs').append('<li><a href="#'+val.InvoiceNumber+'" class="listSmc'+cls+'" data-toggle="tab"> <i class="fa fa-calendar" aria-hidden="true"></i>  '+val.InvoiceDate+'</a></li>');
                      console.log('Active class'+cls);
                       
                       $('.tab-content').append(tabHtml.replace('ref',val.InvoiceNumber));
             $('#'+val.InvoiceNumber).append(highRow); 
             $('#'+val.InvoiceNumber).append(invSumTable);
             $('#'+val.InvoiceNumber+' #invsum').append(invRow);
                        //console.log(listItem.replace("value",val.InvoiceNumber));
                        //console.log("Key Is "+ key + "Value is "+val+"<br>");
                      smcDetails(val.InvoiceNumber,val.BA_ACCOUNT_NO,val.CYCLE_SEQ_NO);
                      
                        
                        
                  });

                var inst = $('[data-remodal-id=bill]').remodal();
                inst.open();
        me.data('running',false);
                

            });
      });

     
      

      function smcDetails(id,account,seqno){

        var url = "http://172.18.108.52:8080/wbapi/wb/fetchbilldetails/"+account+"/"+seqno;
        //console.log(url);
         $.getJSON(url).done(function(data){
          var smctable = '<table class="table table-condensed table-bordered">';
      var tableheader = '<td> <strong>Charge Code</strong></td><td><strong>Description<strong></td><td><strong>Period</strong></td><td><strong>Amount</strong></td><td><strong>GST</strong></td><td><strong>Total</td></tr><tr>';
       
                          var smc_old=null;
              count =0;
                          $.each(data.BillDetails, function(k,val){
                              //console.log(val);
                             
                              var trow='<tr>'
                              $.each(val, function(key,v){
                              //  console.log(v);
                                if(smc_old===null && key==="SMC"){
                                  trow = trow+'<td class="smcclass" colspan="6"> SMC #'+v+'</td></tr><tr>';
                                  smc_old=v;
                                  //console.log('First time'+v);
                                }else{
                                  if(key==="SMC" && smc_old===v){
                                    //console.log('Skipping'+v);
                                  }else if(key==='SMC' && smc_old!==v){
                                     trow = trow+'<td class="smcclass" colspan="6">SMC #'+v+'</td></tr><tr>';
                                     smc_old=v;
                                  }else
                                  { 
                  if(count==0){
                    trow = trow+tableheader;
                    count++;
                  }
                                    trow = trow+'<td>'+v+'</td>';
                                    //console.log('Looks Like '+v);
                                  }
                                }
                              });
                              trow = trow+'</tr>';
                              smctable = smctable+trow;
                          });
                          smctable = smctable+"</table>";
                          $('#'+id).append(smctable);
                          //console.log(smctable);
                        });
      
      }

});

$(function(){

    $('#flash').on('click', function(){
    var accountid = $(this).data('flash');
    var me = $(this);
    if(me.data('running')){
      return false;
    }
    me.data('running',true);
    var acurl = "http://172.18.108.52:8080/wbapi/wb/fetchflashdetails/"+accountid;
    $.getJSON(acurl).done(function(data){
          var count = 0;
          var cls = ""
          $('#myflash').empty();
          var tableRow = '<tr>';
		  var tableHead = '<td><strong>value</strong></td>';
		  var tableData = '<td>value</td>';
          //Loop through JSON 
          //Generate the headers
		  //var obj = data.FlashDetails[0];
		  //console.log(obj);
          
            $.each(data.FlashDetails, function(key,val){
			
              if($.isPlainObject(val)){
				  if(count==0){
					  tableRow = '<thead><tr>';
					  $.each(val,function(k,v){
						  tableRow = tableRow+tableHead.replace('value',k);
					  });
					 tableRow = tableRow+'</tr></thead>';
					 count++;
				 }
				 tableRow = tableRow+'<tr>';
				 $.each(val, function(k,v){
					 tableRow = tableRow+tableData.replace('value',v);
				 });
				 tableRow = tableRow+'</tr>';
			  }
			  else{
				 tableRow = tableRow+tableHead.replace('value',val);
				 
			  }
			 
			 
            });
			 if(count==0){
				   tableRow=tableRow+'</tr>';
			  }
			$('#myflash').append(tableRow);
			me.data('running',false);
			//console.log(tableRow);
			 var inst = $('[data-remodal-id=flashmsg]').remodal();
             inst.open();
         
        });
  });
});

$(function(){

    $('#intrxn').on('click', function(){
    var accountid = $(this).data('intrxn');
    var me = $(this);
    if(me.data('running')){
      return false;
    }
    me.data('running',true);
    var acurl = "http://172.18.108.52:8080/wbapi/wb/fetchintrxninfo/"+accountid;
	
    $.getJSON(acurl).done(function(data){
          var count = 0;
          var cls = ""
          $('#myintrxn').empty();
          var tableRow = '<tr>';
		  var tableHead = '<td><strong>value</strong></td>';
		  var tableData = '<td>value</td>';
          //Loop through JSON 
          //Generate the headers
		  //var obj = data.FlashDetails[0];
		  //console.log(obj);
          
            $.each(data.IntrxnInformation, function(key,val){
			
              if($.isPlainObject(val)){
				  if(count==0){
					  tableRow = '<thead><tr>';
					  $.each(val,function(k,v){
						  tableRow = tableRow+tableHead.replace('value',k);
					  });
					 tableRow = tableRow+'</tr></thead>';
					 count++;
				 }
				 tableRow = tableRow+'<tr>';
				 $.each(val, function(k,v){
					 tableRow = tableRow+tableData.replace('value',v);
				 });
				 tableRow = tableRow+'</tr>';
			  }
			  else{
				 tableRow = tableRow+tableHead.replace('value',val);
				 
			  }
			 
			 
            });
			 if(count==0){
				   tableRow=tableRow+'</tr>';
			  }
			$('#myintrxn').append(tableRow);
			me.data('running',false);
			//console.log(tableRow);
			 var inst = $('[data-remodal-id=intrxnmsg]').remodal();
             inst.open();
         
        });
  });
});


$(function(){

    $('#disconnect').on('click', function(){
    var accountid = $(this).data('disconnect');
    var me = $(this);
    if(me.data('running')){
      return false;
    }
    me.data('running',true);
    var acurl = "http://172.18.108.52:8080/wbapi/wb/FETCHDISCONNECTINFO/"+accountid;
	
    $.getJSON(acurl).done(function(data){
          var count = 0;
          var cls = ""
          $('#mydisconnect').empty();
          var tableRow = '<tr>';
		  var tableHead = '<td><strong>value</strong></td>';
		  var tableData = '<td>value</td>';
          //Loop through JSON 
          //Generate the headers
		  //var obj = data.FlashDetails[0];
		  //console.log(obj);
          
            $.each(data.DisconnectInformation, function(key,val){
			
              if($.isPlainObject(val)){
				  if(count==0){
					  tableRow = '<thead><tr>';
					  $.each(val,function(k,v){
						  tableRow = tableRow+tableHead.replace('value',k);
					  });
					 tableRow = tableRow+'</tr></thead>';
					 count++;
				 }
				 tableRow = tableRow+'<tr>';
				 $.each(val, function(k,v){
					 tableRow = tableRow+tableData.replace('value',v);
				 });
				 tableRow = tableRow+'</tr>';
			  }
			  else{
				 tableRow = tableRow+tableHead.replace('value',val);
				 
			  }
			 
			 
            });
			 if(count==0){
				   tableRow=tableRow+'</tr>';
			  }
			$('#mydisconnect').append(tableRow);
			me.data('running',false);
			//console.log(tableRow);
			 var inst = $('[data-remodal-id=disconnectmsg]').remodal();
             inst.open();
         
        });
  });
});


$(function(){

    $('#fp').on('click', function(){
    var accountid = $(this).data('fp');
    var me = $(this);
    if(me.data('running')){
      return false;
    }
    me.data('running',true);
    var acurl = "http://172.18.108.52:8080/wbapi/wb/FETCHFPINFO/"+accountid;
	console.log(acurl);
    $.getJSON(acurl).done(function(data){
          var count = 0;
          var cls = ""
          $('#myfp').empty();
          var tableRow = '<tr>';
		  var tableHead = '<td><strong>value</strong></td>';
		  var tableData = '<td>value</td>';
          //Loop through JSON 
          //Generate the headers
		  //var obj = data.FlashDetails[0];
		  //console.log(obj);
          
            $.each(data.FreepreviewInformation, function(key,val){
			
              if($.isPlainObject(val)){
				  if(count==0){
					  tableRow = '<thead><tr>';
					  $.each(val,function(k,v){
						  tableRow = tableRow+tableHead.replace('value',k);
					  });
					 tableRow = tableRow+'</tr></thead>';
					 count++;
				 }
				 tableRow = tableRow+'<tr>';
				 $.each(val, function(k,v){
					 tableRow = tableRow+tableData.replace('value',v);
				 });
				 tableRow = tableRow+'</tr>';
			  }
			  else{
				 tableRow = tableRow+tableHead.replace('value',val);
				 
			  }
			 
			 
            });
			 if(count==0){
				   tableRow=tableRow+'</tr>';
			  }
			$('#myfp').append(tableRow);
			me.data('running',false);
			//console.log(tableRow);
			 var inst = $('[data-remodal-id=fpmsg]').remodal();
             inst.open();
         
        });
  });
});



</script>




</body>



</body>

</html>
