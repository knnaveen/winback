<?php
ini_set("date.timezone", "Asia/Kuala_Lumpur");

//include_once('session_header.php');
include_once('function.php');


if(Input::exists('get')){

  $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
	
	
  //$data = file_get_contents("data/data.json");
  $rest = "http://172.18.37.201:8080/WinRest/fetchCampData/".Input::get('XID');
  $data = file_get_contents($rest);
  $jsonObj = json_decode($data,false);
  $output = json_decode($data, true);
  $recoclass;
  
    $time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$finish = $time;
	$total_time = round(($finish - $start), 4);
	

	$time_elapsed_secs = microtime(true) - $start;
	error_log ('LEAD - Host: '.$_SERVER['REMOTE_ADDR'].' '.Input::get('XID').' '.$total_time,0);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<!--<link rel="icon" href="../../favicon.ico">-->

	<title>Editing rows - FooTable</title>

	<!-- Bootstrap core CSS -->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" rel="stylesheet">

	<!-- Prism -->
	<link href="css/prism.css" rel="stylesheet">

	<!-- FooTable Bootstrap CSS -->
	<link href="compiled/footable.bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/docs.css" rel="stylesheet">

	<script src="js/demo-rows.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="docs">
      <div class="splash">
        <div class="color-line"></div>
      
        </div>
		</div>
	
 <div id="header">


            <div class="color-line">
            </div>
            <div id="logo" class="light-version">
                <span>
                    <img src="img/astro_image.png">
                </span>

            </div>
             
           
            <?php generateNavBarNew($userRole,$role,$role2,$displayName); ?>

 </div>

<div class="container">

	<div class="docs-section">
		<?php 
                if(!empty($jsonObj->WinLeadCamp->{'Campaign Information'})){
				?>
				
		<div class="example">
			<?php 
                $toggle = array();
                $panel_class = "highlight";
				
			    $showcols = array("CAMPAIGN" ,
								"PROMO CODE",
								"ACTIVE",
								"VTSTS",
								"MINSUB",
                                  "MINPKG",
                                  "RACE",
                                  "REGION",
                                  "NATIONALITY",
                                  "CUSTOMER",
                                  "Product",
                                  "PAYMENT MTHD",
                                  "Effective Dte",
                                  "ENDDATE"
                                  );

                $pagesize = 'data-page-size="500"';
                generateCampBlockNew("Existing CAMPAIGN",$pagesize,$jsonObj->WinLeadCamp->{'Campaign Information'},$toggle,$showcols); ?>
                 <?php }
              ?>
			<!-- Modal -->
			<div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
				<style scoped>
					/* provides a red astrix to denote required fields */
					.form-group.required .control-label:after {
						content:"*";
						color:red;
						margin-left: 4px;
					}
				</style>
				<div class="modal-dialog" role="document">
					<form class="modal-content form-horizontal" id="editor">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="editor-title">Add Row</h4>
						</div>
						<div class="modal-body">
							<input type="number" id="id" name="id" class="hidden"/>
							<div class="form-group required">
								<label for="firstName" class="col-sm-3 control-label">First Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" required>
								</div>
							</div>
							<div class="form-group required">
								<label for="lastName" class="col-sm-3 control-label">Last Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" required>
								</div>
							</div>
							<div class="form-group">
								<label for="jobTitle" class="col-sm-3 control-label">Job Title</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="jobTitle" name="jobTitle" placeholder="Job Title">
								</div>
							</div>
							<div class="form-group required">
								<label for="startedOn" class="col-sm-3 control-label">Started On</label>
								<div class="col-sm-9">
									<input type="date" class="form-control" id="startedOn" name="startedOn" placeholder="Started On" required>
								</div>
							</div>
							<div class="form-group">
								<label for="dob" class="col-sm-3 control-label">Date of Birth</label>
								<div class="col-sm-9">
									<input type="date" class="form-control" id="dob" name="dob" placeholder="Date of Birth">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Save changes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>

		</div>


	</div>

</div> <!-- /container -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/prism.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Add in any FooTable dependencies we may need -->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<!-- Add in FooTable itself -->
<script src="js/footable.js"></script>
<!-- Initialize FooTable -->
<script>
	jQuery(function($){
		var $modal = $('#editor-modal'),
			$editor = $('#editor'),
			$editorTitle = $('#editor-title'),
			ft = FooTable.init('#editing-example', {
				editing: {
					enabled: true,
					addRow: function(){
						$modal.removeData('row');
						$editor[0].reset();
						$editorTitle.text('Add a new row');
						$modal.modal('show');
					},
					editRow: function(row){
						var values = row.val();
						$editor.find('#id').val(values.id);
						$editor.find('#firstName').val(values.firstName);
						$editor.find('#lastName').val(values.lastName);
						$editor.find('#jobTitle').val(values.jobTitle);
						$editor.find('#startedOn').val(values.startedOn.format('YYYY-MM-DD'));
						$editor.find('#dob').val(values.dob.format('YYYY-MM-DD'));

						$modal.data('row', row);
						$editorTitle.text('Edit row #' + values.id);
						$modal.modal('show');
					},
					deleteRow: function(row){
						if (confirm('Are you sure you want to delete the row?')){
							row.delete();
						}
					}
				}
			}),
			uid = 10;

		$editor.on('submit', function(e){
			if (this.checkValidity && !this.checkValidity()) return;
			e.preventDefault();
			var row = $modal.data('row'),
				values = {
					id: $editor.find('#id').val(),
					firstName: $editor.find('#firstName').val(),
					lastName: $editor.find('#lastName').val(),
					jobTitle: $editor.find('#jobTitle').val(),
					startedOn: moment($editor.find('#startedOn').val(), 'YYYY-MM-DD'),
					dob: moment($editor.find('#dob').val(), 'YYYY-MM-DD')
				};

			if (row instanceof FooTable.Row){
				row.val(values);
			} else {
				values.id = uid++;
				ft.rows.add(values);
			}
			$modal.modal('hide');
		});
	});
</script>
</body>
</html>