<?php
function generateNavBar($userRole,$role, $role2,$displayName){
$placeholder = "";
if($role=="lead"){
  $placeholder ="Search Lead";
}else{
  $placeholder = "Search Account";
}
echo '<nav role="navigaton">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><div class="timedata"><i class="fa fa-2x fa-clock-o" aria-hidden="true"></i>
'.date('d-M-y H:i:s').'
                </div></li>

                <li>';
                if($role=="lead"){
                echo '<a id="customlink" href="#alert" class=""> <i class="fa fa-bell-o" aria-hidden="true"></i>Alerts</a>
                </li>';
                }
                
                echo '<li>
                    <form method="get"  class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" name="XID" class="form-control" placeholder="'.$placeholder.'"/>
                        </div>
                        <button class="btn btn-default" role="submit" type="submit"><i class="fa fa-search"></i></button>
                    </form>

                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">  Welcome  '.$displayName. '<i class="fa fa-sign-out"></i></a>
                    <ul class="dropdown-menu">
                          <li><a href="profle.php">MyProfile</a></li>
                          <li><a href="logout.php">Logout</a></li>';
                           if($userRole=="admin"){
                            echo '<li><a href="userMgmt.php">User Management</a></li>';
							
                            echo '<li><a href="campMgmt.php">Campaign </a></li>';
							echo '<li><a href="alertMgmt.php">Alerts </a></li>';
							
							}

						echo '
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>';
}
function generateNavBarNew($userRole,$role, $role2,$displayName,$searchSTR){
$placeholder = "";
if($role=="lead"){
  $placeholder =$searchSTR;
}else{
  $placeholder = $searchSTR;
}
echo '<nav role="navigaton">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><div class="timedata"><i class="fa fa-2x fa-clock-o" aria-hidden="true"></i>
'.date('d-M-y H:i:s').'
                </div></li>

                <li>';
				
                
                
                echo '<li>
                    <form method="get"  class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" name="XID" class="form-control" placeholder="'.$placeholder.'"/>
                        </div>
                        <button class="btn btn-default" role="submit" type="submit"><i class="fa fa-search"></i></button>
                    </form>

                </li>
               <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">  Welcome  '.$displayName. '<i class="fa fa-sign-out"></i></a>
                    <ul class="dropdown-menu">
                          <li><a href="profle.php">MyProfile</a></li>
                          <li><a href="logout.php">Logout</a></li>';
                           if($userRole=="admin"){
                            echo '<li><a href="userMgmt.php">User Management</a></li>';
							}
							if($userRole=="admin"){
                            echo '<li><a href="lead.php">Home</a></li>';
							echo '<li><a href="campMgmt.php">Campaign </a></li>';
							echo '<li><a href="alertMgmt.php">Alerts </a></li>';
							
							}

						echo '
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>';
}
?>

