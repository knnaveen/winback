<?php
include_once('config/init.php');
include_once('navbar.php');
$displayName =null;
$role = null;
$role2 = null;
$userRole = null;
if(Cookie::exists(Config::get('logged/cookie_name'))&&(!Session::exists(Config::get('session/session_name')))){
		$cookieval = Cookie::get(Config::get('logged/cookie_name'));
		$pieces = explode('|',$cookieval);
		$cookiehash = $pieces[0];
		$dbhash = DB::getInstance()->get('table_user_sessions',array(
            array('username','=',$pieces[1]),
            array('agent'=> $_SERVER['HTTP_USER_AGENT'])
            ));
		$storedhash = Hash::make($cookiehash,$dbhash->first()->salt);
		if($dbhash->first()->hash != $storedhash){
			 echo "Redirecting";
			 Redirect::to('login.php');
		}else{
      $displayName = $pieces[2];
      $role2 = $pieces[4];
      $userRole = $pieces[5];
	}

}else if(Session::exists(Config::get('session/session_name'))){
        // nothing to do
        $displayName = Session::get(Config::get('session/displayName'));
        $role2 = Session::get(Config::get('session/session_role2'));
        $role = Session::get(Config::get('session/session_role'));
        $userRole = Session::get(Config::get('session/userRole'));

}else{
    Redirect::to('login.php');
}
?>
