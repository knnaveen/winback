<?php
header( 'Access-Control-Allow-Origin: *' );
header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
header( 'Access-Control-Allow-Credentials: true' );


header('Content-Type: application/json');

error_reporting(E_ERROR);

include_once('functions.php');

$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$input = json_decode(file_get_contents('php://input'),true);


// retrieve the table and key from the path
$table = preg_replace('/[^a-z0-9_]+/i','',array_shift($request));
$request1=$request;
$key = array_shift($request)+0;
$key1 = array_shift($request)+0;
$key3 = array_shift($request1);





if ($method == 'GET') {
	
		$conn=getConnection();

        $isOK=true;
        $retArr;
        $sql;

	      switch (strtoupper($table)) {
              case "FETCHCUSTINFO":
                     $sql="select * from dbo.wb_cust_info('".$key."')";
	                 $retArr=getDataSqlODBC($conn,$sql,'CustomerInformation');
                     break;
              
              case "FETCHACCOUNTINFO":
                     $sql="select * from dbo.wb_acc_info('".$key."')";
	                 $retArr=getDataSqlODBC($conn,$sql,'AccountInformation');
                     break;
              case "FETCHADDRESSINFO":
                     $sql="select * from dbo.wb_address_info('".$key."')";
	                 $retArr=getDataSqlODBC($conn,$sql,'AddressInformation');
                     break;   
              
              case "FETCHCOLLECTIONINFO":
                     $sql="select * from dbo.wb_coll_bucket('".$key."')";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'CollectionBucket');
                     break;
              
              case "FETCHSMCINFO": 
                     $sql="select * from dbo.wb_smc_info('".$key."')";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'SMCInformation');
                     break;
              
              case "FETCHPAYMENTINFO":
                     $sql="select * from dbo.wb_payment_info('".$key."') order by convert(datetime,DepositDt,103) desc";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'PaymentInformation');
                     break;
                  
              case "FETCHFININFO":
                     $sql="select * from dbo.wb_fin_info('".$key."') where  amount>=1 order by convert(datetime,PostDt,103) desc";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'FinancialInformation');
                     break;
              
              case "FETCHINTRXNINFO":
                     $sql="select * from dbo.wb_intrxn_info('".$key."') order by convert(datetime,start_time,103) desc";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'IntrxnInformation');
                     break;
              
              
              
              
              case "FETCHDISCONNECTINFO":
                     $sql="select * from dbo.wb_disconnect_info('".$key."') order by convert(Datetime,Date,103) desc,1";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'DisconnectInformation');
                     break;
                  
             case "FETCHFPINFO":
                     $sql="select * from dbo.wb_fp_info('".$key."')  order by 1,5,6";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'FreepreviewInformation');
                     break;
              
              
              case "FETCHBILLINFO":
                     $sql="select * from dbo.wb_bill_info('".$key."') order by convert(datetime,InvoiceDate,103) desc";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'BillInformation');
                     break;
                  
              case "FETCHFLASHDETAILS":
                     $sql="select * from dbo.wb_flash_details(".$key.") order by convert(date,StartDate,103) desc";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'FlashDetails');
                     break;
              
              
		    case "FETCHBILLDETAILS":
                     $sql="select * from dbo.wb_bill_details('".$key."','".$key1."') order by 1 desc,5 desc,2";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'BillDetails');
                     break;
		   
		    case "FETCHORDERSTATUS":
                     $sql="select * from dbo.orderstatus('".$key."')";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'OrderStatus');
                     break;
			
			case "FETCHCASESTATUS":
                     $sql="select * from dbo.casestatus('".$key."')";
                     
	                 $retArr=getDataSqlODBC($conn,$sql,'CaseStatus');
                     break;
			case "FETCHACCOUNTBYPHONE":
					 $sql="select * from dbo.search_account_by_phone('".$key3."')";
					 $retArr=getDataSqlODBC($conn,$sql,'AccountListByPhone');
                     break;
			case "FETCHACCOUNTBYXID":
					 $sql="select * from dbo.search_account_by_x_id('".$key3."')";
					 $retArr=getDataSqlODBC($conn,$sql,'AccountListByXID');
                     break;
			case "FETCHWBAPIINFO":
                     $retArr=getWBApiInfo($key);
                     break;      
                  
		      default: $this->header( 'HTTP/1.1 400: BAD REQUEST' );
                       $isOK=false;

		}
		
           sqlsrv_close($conn);

	      if (isOK==true){
			 
		      exit(json_encode($retArr));
	           
		 }
		 
		
	  
}else{
	
	$this->header( 'HTTP/1.1 405: Method not allowed' );
}
	
     

function getWBApiInfo($key){
    
    
    $start = microtime(true);
                $retArr=array();
               
                //error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
               
 
                if(!empty($key)){
                               
                    $nodes = array();
                               
                    $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHCUSTINFO/".$key;
                    $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHACCOUNTINFO/".$key;
                    $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHADDRESSINFO/".$key;
                    $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHCOLLECTIONINFO/".$key;
                    $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHSMCINFO/".$key;
                    $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHPAYMENTINFO/".$key;
                    $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHFININFO/".$key;
                    //$nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHINTRXNINFO/".$key;
                   // $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHDISCONNECTINFO/".$key;
                   // $nodes[]="http://172.18.108.52:8080/wbapi/wb/FETCHFPINFO/".$key;
               
                                
                    $node_count = count($nodes);
 
                    $curl_arr = array();
                    $master = curl_multi_init();

                    for($i = 0; $i < $node_count; $i++)
                    {
                        $url =$nodes[$i];
                        $curl_arr[$i] = curl_init($url);
                        curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
                        curl_multi_add_handle($master, $curl_arr[$i]);
                    }

                    /*
                    do {
                         curl_multi_exec($master,$running);
                    } while($running > 0);
                    */
                    
                    full_curl_multi_exec($master, $still_running); // start requests 
                    
                    do { // "wait for completion"-loop 
                        curl_multi_select($master); // non-busy (!) wait for state change 
                        full_curl_multi_exec($master, $still_running); // get new state 
                        while ($info = curl_multi_info_read($master)) { 
                          // process completed request (e.g. curl_multi_getcontent($info['handle'])) 
                        } 
                      } while ($still_running); 

                    $iArr=array();

                    for($i = 0; $i < $node_count; $i++)
                    {
                        $results = curl_multi_getcontent  ( $curl_arr[$i]  );
                        $retArr[]=json_decode($results,true);
                        if (!empty(json_decode($results))){
                                        //var_dump(json_decode(($results),true));
                                        $iArr=array_merge($iArr,json_decode(($results),true));
                        }

                    }

                               
                }
               
               
                $time_elapsed_secs = microtime(true) - $start;
               
                return (array('WinBack' => $iArr));
               
}


function full_curl_multi_exec($mh, &$still_running) { 
    do { 
      $rv = curl_multi_exec($mh, $still_running); 
    } while ($rv == CURLM_CALL_MULTI_PERFORM); 
    return $rv; 
  } 


      

?>