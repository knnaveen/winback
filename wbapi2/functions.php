<?php

function getConnection(){

  $serverName = "172.18.56.180"; //serverName\instanceName, portNumber (default is 1433)
  $connectionInfo = array( "Database"=>"IMPODS01", "UID"=>"astroda01", "PWD"=>"AstroDa@16");
  $conn = sqlsrv_connect( $serverName, $connectionInfo);

  if( $conn ) {

  }else{
       die( print_r("<h2>Database server not available. Please try again after sometime!</h2><hr>", true));
  }

  return $conn;

}



function getDataSqlODBC($conn,$sql,$Header){

	$r_array=array();

	$stmt = sqlsrv_query( $conn, $sql );

	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		
		foreach($row as &$value){
			$value = mb_convert_encoding($value, "UTF-8", "Windows-1252");
		  }
		unset($value); # safety: remove reference

      	$r_array[]=$row;
	}
	

	if (empty($r_array)==true){
		
		sqlsrv_free_stmt( $stmt);
		$stmt = sqlsrv_prepare($conn, $sql);
		$Hdr = array();
		foreach(sqlsrv_field_metadata( $stmt ) as $fieldMetadata ) {
        		//$Hdr[]=array($fieldMetadata['Name'] => ' ');
			$Hdr[]=$fieldMetadata['Name'];

      	}
		$r_array=$Hdr;
		sqlsrv_free_stmt( $stmt);

	}else {
		sqlsrv_free_stmt( $stmt);
	}
	return (array($Header=>$r_array));



}

function send_cors_headers( $headers ) {
		$headers['Access-Control-Allow-Origin']      = get_http_origin(); // Can't use wildcard origin for credentials requests, instead set it to the requesting origin
		$headers['Access-Control-Allow-Credentials'] = 'true';
		// Access-Control headers are received during OPTIONS requests
		if ( 'OPTIONS' == $_SERVER['REQUEST_METHOD'] ) {
			if ( isset( $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] ) ) {
				$headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS';
			}
			if ( isset( $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'] ) ) {
				$headers['Access-Control-Allow-Headers'] = $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'];
			}
		}
		return $headers;
	}

?>